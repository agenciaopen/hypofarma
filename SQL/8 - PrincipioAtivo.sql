use dbHypofarma
create table principio_ativo
  (num_principio_ativo int not null AUTO_INCREMENT,
   desc_principio_ativo varchar(100),
   idc_ativo char(1),
   num_usuario_cadastro int,
   primary key (num_principio_ativo),
   foreign key (num_usuario_cadastro) references usuario(id))
--
DELIMITER $
create procedure  spuPrincipioAtivo_ListaTodos()
begin
   select num_principio_ativo as Id,
          desc_principio_ativo as DescPrincipioAtivo,
		  case idc_ativo when 'A' then 'Ativo' else 'Inativo' end as IdcSituacao,
          num_usuario_cadastro as NumUsuarioCadastro
   from principio_ativo;
end$
--
DELIMITER $
create procedure spuPrincipioAtivo_Insere(DESCRICAO varchar(100), ID_USUARIO int )
begin
   insert into principio_ativo (desc_principio_ativo, idc_ativo, num_usuario_cadastro)
   values (ltrim(rtrim(DESCRICAO)), 'A',  ID_USUARIO);
end$
--
DELIMITER $
create procedure spuPrincipioAtivo_Altera(ID_PRINCIPIO int, DESCRICAO varchar(100), ATIVO char(1) )
begin
   update principio_ativo 
   set desc_principio_ativo = ltrim(rtrim(DESCRICAO)),
		idc_ativo = ATIVO
   where num_principio_ativo = ID_PRINCIPIO;
end$
--
DELIMITER $
create procedure spuPrincipioAtivo_Exclui(ID int )
begin
   delete from principio_ativo 
   where num_principio_ativo = ID;
end$