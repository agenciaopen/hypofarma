use dbHypofarma
--
create table estado
  (num_estado integer not null,
   sigla_estado char(2),
   desc_estado varchar(100),
  primary key (num_estado),
  constraint uk1_estado unique (sigla_estado)) 
 --
DELIMITER $
create procedure spuEstado_ListaTodos()
begin  
   select 	num_estado as Id, 
			sigla_estado as Sigla, 
            desc_estado as Nome
   from estado;
end$ 
--
DELIMITER $
create procedure spuEstado_Insere(ID_ESTADO int, SIGLA char(2), DESCRICAO varchar(100))
begin  
   insert into estado (num_estado, sigla_estado, desc_estado)
   values (ID_ESTADO, SIGLA, ltrim(rtrim(DESCRICAO)));
end$ 
--
create table municipio
  (num_municipio integer not null,
   desc_municipio varchar(100),
   num_estado integer,
   primary key (num_municipio),
   FOREIGN KEY (num_estado) REFERENCES estado(num_estado) ON DELETE CASCADE)
--
DELIMITER $
create procedure spuMunicipio_ListaPorEstado(ID_ESTADO int)
begin
   select 	num_municipio as Id,
			desc_municipio as Nome,
			num_estado as NumEstado
	from municipio
    where num_estado = ID_ESTADO;
end$ 
--
DELIMITER $
create procedure spuMunicipio_ListaTodos()
begin
   select 	num_municipio as Id,
			desc_municipio as Nome,
			num_estado as NumEstado
	from municipio;
end$ 
--
DELIMITER $
create procedure spuMunicipio_Insere(ID_MUNICIPIO int, DESCRICAO varchar(100), ID_ESTADO int)
begin
   insert into municipio(num_municipio, desc_municipio, num_estado)
   values (ID_MUNICIPIO, ltrim(rtrim(DESCRICAO)), ID_ESTADO);
end$ 