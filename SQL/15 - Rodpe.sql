create table midia_web
  (cod_canal_web int not null,
   link_perfil varchar(255),
   primary key (cod_canal_web))
--
DELIMITER $
create procedure spuMidiaWeb_ListaTodos()
begin
   select cod_canal_web as CanalWeb,
		  link_perfil as LinkPerfil
	from midia_web;
end$
--
DELIMITER $
create procedure spuMidiaWeb_Insere(ID_CANAL int, LNK_PERFIL varchar(255))
begin
   insert into midia_web (cod_canal_web, link_perfil)
   values (ID_CANAL, LTRIM(RTRIM(LNK_PERFIL)));
end$
--
DELIMITER $
create procedure spuMidiaWeb_Exclui(ID_CANAL int)
begin
   delete from midia_web 
   where cod_canal_web = ID_CANAL;
end$
