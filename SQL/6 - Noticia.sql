use dbHypofarma
--
create table noticia
  (num_noticia int not null AUTO_INCREMENT,
   titulo_noticia varchar(256),
   desc_noticia varchar(8000),
   idc_ativo char(1),
   dth_cadastro timestamp,
   num_usuario_cadastro int,
   primary key (num_noticia),
   foreign key (num_usuario_cadastro) references usuario(id))
--
DELIMITER $
create procedure spuNoticia_ListaTodos()
begin
   select num_noticia as Id,
		  titulo_noticia as TituloNoticia,
		  desc_noticia as DescNoticia,
          case idc_ativo when 'A' then 'Ativo' else 'Inativo' end as IdcSituacao,
          dth_cadastro as DthCadastro,
          num_usuario_cadastro as NumUsuarioCadastro
	from noticia;
end$
--
DELIMITER $
create procedure spuNoticia_Insere(TITULO varchar(256), DESCRICAO varchar(8000), DT_CADASTRO timestamp, ID_USUARIO_CADASTRO int)
begin
   insert into noticia (titulo_noticia, desc_noticia, idc_ativo, dth_cadastro, num_usuario_cadastro)
   values (ltrim(rtrim(TITULO)), ltrim(rtrim(DESCRICAO)), 'A', DT_CADASTRO, ID_USUARIO_CADASTRO);
end$
--
DELIMITER $
create procedure spuNoticia_Altera(ID_NOTICIA int, TITULO varchar(256), DESCRICAO varchar(8000), ATIVO char(1))
begin
   update noticia 
   set 	titulo_noticia = ltrim(rtrim(TITULO)),
        desc_noticia = ltrim(rtrim(DESCRICAO)), 
		idc_ativo = ATIVO
   where num_noticia = ID_NOTICIA;
end$
--
DELIMITER $
create procedure spuNoticia_Exclui(ID int)
begin
   delete from noticia 
   where num_noticia = ID;
end$
--
create table noticia_img
   (num_img integer not null,
    num_noticia integer not null,
    nom_arquivo varchar(100),
    img_noticia MEDIUMBLOB,
    primary key (num_img, num_noticia),
    foreign key (num_noticia) references noticia(num_noticia))
--
DELIMITER $
create procedure spuNoticiaImg_ListaPorNoticia(ID_NOTICIA int)
begin
   select 	num_img as NumImg,
			num_noticia as Id,
            nom_arquivo as NomArquivo,
            img_noticia ImgNoticia
	from noticia_img
    where num_noticia = ID_NOTICIA;
end$
--
DELIMITER $
create procedure spuNoticiaImg_Insere(ID_NOTICIA int, NOME_ARQUIVO varchar(100), IMG MEDIUMBLOB)
begin
   declare NUM_NOTICIA_IMG int;   
   select count(num_img) + 1 
   into NUM_NOTICIA_IMG 
   from noticia_img 
   where num_noticia = ID_NOTICIA;
   
   insert into noticia_img (num_img, num_noticia, nom_arquivo, img_noticia)
   values (NUM_NOTICIA_IMG, ID_NOTICIA, NOME_ARQUIVO, IMG);
end$
--
DELIMITER $
create procedure spuNoticiaImg_Exclui(ID_NOTICIA int, ID_ARQUIVO int)
begin
  
   delete from noticia_img 
   where num_img = ID_ARQUIVO
   and num_noticia = ID_NOTICIA;   
end$