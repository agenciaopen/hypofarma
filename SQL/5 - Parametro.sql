use dbHypofarma
--
create table parametro_sistema
  (cod_parametro varchar(100) not null,
   desc_parametro varchar(100) not null,
   val_parametro varchar(1000) not null,
   constraint uk_parametro unique(cod_parametro))
--
DELIMITER $
create procedure spuPArametroSistema_ListaTodos()
begin
    select 	cod_parametro as CodParametro,
			desc_parametro as DescParametro,
            val_parametro as ValParametro
    from parametro_sistema;
end$ 