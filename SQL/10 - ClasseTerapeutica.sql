use dbHypofarma
--
create table classe_terapeutica
  (num_classe_terapeutica int not null AUTO_INCREMENT,
   desc_classe_terapeutica varchar(100),
   idc_ativo char(1),
   num_usuario_cadastro int,
   check (idc_ativo = 'A' or idc_ativo = 'I'),
   primary key (num_classe_terapeutica),
   foreign key (num_usuario_cadastro) references usuario(id))
--
DELIMITER $
create procedure  spuClasseTerapeutica_ListaTodos()
begin
   select num_classe_terapeutica as Id,
          desc_classe_terapeutica as DescClasseTerapeutica,
		  case idc_ativo when 'A' then 'Ativo' else 'Inativo' end as IdcSituacao,
          num_usuario_cadastro as NumUsuarioCadastro
   from classe_terapeutica;
end$
--
DELIMITER $
create procedure spuClasseTerapeutica_Insere(DESCRICAO varchar(100), ID_USUARIO int )
begin
   insert into classe_terapeutica (desc_classe_terapeutica, idc_ativo, num_usuario_cadastro)
   values (ltrim(rtrim(DESCRICAO)), 'A',  ID_USUARIO);
end$
--
DELIMITER $
create procedure spuClasseTerapeutica_Altera(ID_CLASSE int, CLASSE varchar(100), ATIVO char(1) )
begin
   update classe_terapeutica 
   set desc_classe_terapeutica = ltrim(rtrim(CLASSE)),
		idc_ativo = ATIVO
   where num_classe_terapeutica = ID_CLASSE;
end$
--
DELIMITER $
create procedure spuClasseTerapeutica_Exclui(ID int )
begin
   delete from classe_terapeutica 
   where num_classe_terapeutica = ID;
end$