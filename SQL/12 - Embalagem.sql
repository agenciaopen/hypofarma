use dbHypofarma
--
create table embalagem
  (num_embalagem  int not null AUTO_INCREMENT,
   desc_embalagem varchar(100),
   arq_icone_embalagem  MEDIUMBLOB,
   idc_ativo char(1),
   num_usuario_cadastro int,
   primary key (num_embalagem),
   foreign key (num_usuario_cadastro) references usuario(id))
--
DELIMITER $
create procedure spuEmbalagem_ListaTodos()
begin
   select num_embalagem as Id,
          desc_embalagem as DescEmbalagem,
          arq_icone_embalagem as ArqIconeEmbalagem,
          case idc_ativo when 'A' then 'Ativo' else 'Inativo' end as IdcSituacao,
          num_usuario_cadastro as NumUsuarioCadastro
   from embalagem;
end$
--
DELIMITER $
create procedure spuEmbalagem_Insere(DESCRICAO varchar(100), ARQ_ICONE MEDIUMBLOB, ID_USUARIO int)
begin
   insert into embalagem (desc_embalagem, arq_icone_embalagem, idc_ativo, num_usuario_cadastro)
   values (ltrim(rtrim(DESCRICAO)), ARQ_ICONE, 'A', ID_USUARIO);
end$
--
DELIMITER $
create procedure spuEmbalagem_Altera(ID_EMBALAGEM int, DESCRICAO varchar(100), ARQ_ICONE MEDIUMBLOB, ATIVO char(1))
begin
   update embalagem 
   set desc_embalagem = ltrim(rtrim(DESCRICAO)), 
       arq_icone_embalagem = ARQ_ICONE, 
       idc_ativo = ATIVO
	where num_embalagem = ID_EMBALAGEM;
end$
--
DELIMITER $
create procedure spuEmbalagem_Exclui(ID int)
begin
   delete from embalagem 
   where num_embalagem = ID;
end$