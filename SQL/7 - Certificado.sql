use dbHypofarma
--
create table certificado_analise
  (num_certificado int not null AUTO_INCREMENT,
   nom_arq_certificado varchar(100),
   arq_certificado MEDIUMBLOB,
   num_usuario_cadastro integer,
   primary key (num_certificado),
   foreign key (num_usuario_cadastro) references usuario(id))
--
DELIMITER $
create procedure spuCertificadoAnalise_ListaTodos()
begin
    select 	num_certificado as Id,
			nom_arq_certificado as NomArquCertificado,
            arq_certificado as ArqCertificado,
            num_usuario_cadastro as NumUsuarioCadastro
    from certificado_analise;
end$ 
--
DELIMITER $
create procedure spuCertificadoAnalise_ListaPeloId(ID_CERTIFICADO int)
begin
    select 	num_certificado as Id,
			nom_arq_certificado as NomArquCertificado,
            arq_certificado as ArqCertificado,
            num_usuario_cadastro as NumUsuarioCadastro
    from certificado_analise
    where num_certificado = ID_CERTIFICADO;
end$ 
--
DELIMITER $
create procedure spuCertificadoAnalise_ListaPeloNomArquivo(NOM_ARQUIVO varchar(100))
begin
    select 	num_certificado as Id,
			nom_arq_certificado as NomArquCertificado,
            arq_certificado as ArqCertificado,
            num_usuario_cadastro as NumUsuarioCadastro
    from certificado_analise
    where lower(rtrim(rtrim(nom_arq_certificado))) = lower(rtrim(rtrim(NOM_ARQUIVO)));
end$ 
--
DELIMITER $
create procedure spuCertificadoAnalise_Insere(NOM_ARQUIVO varchar(100), ARQUIVO MEDIUMBLOB, ID_USUARIO_CADASTRO int)
begin
    insert into certificado_analise (nom_arq_certificado, arq_certificado, num_usuario_cadastro)
    values (ltrim(rtrim(NOM_ARQUIVO)), ARQUIVO, ID_USUARIO_CADASTRO);
end$ 
--
DELIMITER $
create procedure spuCertificadoAnalise_Exclui(ID int)
begin
    delete from certificado_analise 
    where num_certificado = ID;
end$ 