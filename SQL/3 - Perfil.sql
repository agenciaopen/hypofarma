use dbHypofarma
--
create table perfil_acesso
  (num_perfil int not null AUTO_INCREMENT,
   desc_perfil varchar(100),
   idc_admin char(1),
   primary key (num_perfil))
--
DELIMITER $
create procedure spuPerfilAcesso_ListaTodos()
begin
   select num_perfil as Id,
          desc_perfil as DescPerfil,
          case idc_admin when 'S' then 1 else 0 end as IdcAdmin
   from perfil_acesso;
end$