use dbHypofarma
--
--
create table categoria_regulatoria
  (num_categoria_regulatoria int not null AUTO_INCREMENT,
   desc_categoria_regulatoria varchar(100),
   idc_ativo char(1),
   num_usuario_cadastro int,
   primary key (num_categoria_regulatoria),
   foreign key (num_usuario_cadastro) references usuario(id))
--
DELIMITER $
create procedure  spuCategoriaRegulatoria_ListaTodos()
begin
   select num_categoria_regulatoria as Id,
          desc_categoria_regulatoria as DescCategoriaRegulatoria,
		  case idc_ativo when 'A' then 'Ativo' else 'Inativo' end as IdcSituacao,
          num_usuario_cadastro as NumUsuarioCadastro
   from categoria_regulatoria;
end$
--
DELIMITER $
create procedure spuCategoriaRegulatoria_Insere(DESCRICAO varchar(100), ID_USUARIO int )
begin
   insert into categoria_regulatoria (desc_categoria_regulatoria, idc_ativo, num_usuario_cadastro)
   values (ltrim(rtrim(DESCRICAO)), 'A',  ID_USUARIO);
end$
--
DELIMITER $
create procedure spuCategoriaRegulatoria_Altera(ID_CATEGORIA int, DESCRICAO varchar(100), ATIVO char(1) )
begin
   update categoria_regulatoria 
   set desc_categoria_regulatoria = ltrim(rtrim(DESCRICAO)),
		idc_ativo = ATIVO
   where num_categoria_regulatoria = ID_CATEGORIA;
end$
--
DELIMITER $
create procedure spuCategoriaRegulatoria_Exclui(ID int )
begin
   delete from categoria_regulatoria 
   where num_categoria_regulatoria = ID;
end$