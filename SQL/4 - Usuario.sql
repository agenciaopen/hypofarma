use dbHypofarma
--
create table usuario
(id 			integer not null AUTO_INCREMENT,
 nom_usuario 	varchar(100) not null,
 des_senha 		varchar(100) not null ,
 e_mail 		varchar(100) not null,
 idc_situacao 	char(1) not null,
 primary key (id))
 --
DELIMITER $
create procedure spuUsuario_ListaTodos()
begin
    select 	id as Id,
			nom_usuario as NomUsuario,
            des_senha as DesSenha,
            e_mail as EMail,
            case idc_situacao when 'A' then 'Ativo' else 'Inativo' end as IdcSituacao
	from usuario;
end$ 
--
DELIMITER $
create procedure spuUsuario_ListaPeloId(USUARIO_ID int)
begin
    select 	id as Id,
			nom_usuario as NomUsuario,
            des_senha as DesSenha,
            e_mail as EMail,
            case idc_situacao when 'A' then 'Ativo' else 'Inativo' end as IdcSituacao
	from usuario
    where id = USUARIO_ID;
end$ 
--
DELIMITER $
create procedure spuUsuario_ListaPeloEMail(EMAIL varchar(100))
begin
    select 	id as Id,
			nom_usuario as NomUsuario,
            des_senha as DesSenha,
            e_mail as EMail,
            case idc_situacao when 'A' then 'Ativo' else 'Inativo' end as IdcSituacao
	from usuario
    where ltrim(rtrim(lower(e_mail))) = ltrim(rtrim(lower(EMAIL)));
end$ 
--
DELIMITER $
create procedure spuUsuario_Insere(NOME varchar(100), SENHA varchar(100), EMAIL varchar(100))
begin
   insert into usuario (nom_usuario, des_senha, e_mail, idc_situacao)
   values (ltrim(rtrim(NOME)), ltrim(rtrim(SENHA)), ltrim(rtrim(EMAIL)), 'A');
end$ 
--
DELIMITER $
create procedure spuUsuario_Altera(USUARIO_ID integer, NOME varchar(100), SENHA varchar(100), EMAIL varchar(100), SITUACAO CHAR(1))
begin
   update usuario 
   set nom_usuario = ltrim(rtrim(NOME)), 
		des_senha = ltrim(rtrim(SENHA)),
        e_mail = ltrim(rtrim(EMAIL)), 
        idc_situacao = SITUACAO
   where id = USUARIO_ID;
end$ 
--
 create table usuario_log
   (
    usuario_id 	integer not null,
    num_log 	integer not null,
    dth_log 	TIMESTAMP ,
    desc_log 	varchar(500) not null,
    primary key (usuario_id, num_log),
    FOREIGN KEY (usuario_id) REFERENCES usuario(id) ON DELETE CASCADE
    )
--
DELIMITER $
create procedure spuUsuarioLog_ListaPeloUsuarioId(ID_USUARIO integer)
begin
    select 	num_log as Id,
			usuario_id as NumUsuario,
			dth_log as DataLog,
            desc_log as DescLog
    from usuario_log
    where usuario_id = ID_USUARIO;
end$ 
--
DELIMITER $
create procedure spuUsuarioLog_Insere(ID_USUARIO integer, DT_LOG TIMESTAMP, DESCLOG varchar(500))
begin
   declare NUMLOG int;
   
   select count(usuario_id) + 1 
   into NUMLOG 
   from usuario_log 
   where usuario_id = ID_USUARIO;
   
   insert into usuario_log (usuario_id, num_log, dth_log, desc_log)
   values (	ID_USUARIO, NUMLOG, DT_LOG, ltrim(rtrim(DESCLOG)));
end$ 
--
create table usuario_telefone
   (num_usuario_telefone 	integer not null,
    usuario_id 				integer not null,
    num_telefone 			char(11),
    num_tipo_telefone 		integer,
    idc_whatsapp 			char(1) default 'N',
    primary key (usuario_id, num_usuario_telefone),
    FOREIGN KEY (usuario_id) REFERENCES usuario(id) ON DELETE CASCADE)
--
DELIMITER $
create procedure spuUsuarioTelefone_ListaPeloUsuarioId(ID_USUARIO integer)
begin
    select 	num_usuario_telefone as Id,
			usuario_id as NumUsuario,
			num_telefone as NumTelefone,
            num_tipo_telefone as TipoTelefone,
            case idc_whatsapp when 'S' then 1 else 0 end as IdcWhatsapp
    from usuario_telefone
    where usuario_id = ID_USUARIO;
end$ 
 --
DELIMITER $
create procedure spuUsuarioTelefone_Insere(ID_USUARIO integer, TELEFONE_NUM CHAR(11), TIPO_TELEFONE int, IDCWHATSAPP CHAR(1))
begin
   declare NUM_USUARIO_TEL int;
   
   select count(usuario_id) + 1 
   into NUM_USUARIO_TEL 
   from usuario_telefone 
   where usuario_id = USUARIO_ID;
   
   insert into usuario_telefone (num_usuario_telefone, usuario_id, num_telefone, num_tipo_telefone, idc_whatsapp)
   values (NUM_USUARIO_TEL, ID_USUARIO, TELEFONE_NUM, TIPO_TELEFONE, IDCWHATSAPP);
end$ 
 --
DELIMITER $
create procedure spuUsuarioTelefone_Exclui(ID_USUARIO integer, NUMTELEFONE CHAR(11))
begin  
   delete from usuario_telefone 
   where usuario_id = ID_USUARIO
   and num_telefone = NUMTELEFONE;
end$ 
--
create table usuario_documento
(
  num_documento integer not null,
  usuario_id integer not null,
  cod_documento varchar(100),
  num_tipo_documento integer not null,
  primary key (usuario_id, num_documento),
  FOREIGN KEY (usuario_id) REFERENCES usuario(id) ON DELETE CASCADE
 )
 --
DELIMITER $
create procedure spuUsuarioDocumento_ListaPeloUsuarioId(ID_USUARIO integer)
begin
    select 	num_documento as Id,
			usuario_id as NumUsuario,
			cod_documento as CodDocumento,
            num_tipo_documento as TipoDocumento
    from usuario_documento
    where usuario_id = ID_USUARIO;
end$ 
 --
DELIMITER $
create procedure spuUsuarioDocumento_PeloCodigo(COD_DOC varchar(100))
begin
    select 	num_documento as Id,
			usuario_id as NumUsuario,
			cod_documento as CodDocumento,
            num_tipo_documento as TipoDocumento
    from usuario_documento
    where cod_documento = COD_DOC;
end$ 
 --
DELIMITER $
create procedure spuUsuarioDocumento_Insere(ID_USUARIO integer, DOCUMENTO_COD varchar(100), TIPO_DOCUMENTO int)
begin
   declare NUM_USUARIO_DOCUMENTO int;
   
   select count(usuario_id) + 1 
   into NUM_USUARIO_DOCUMENTO 
   from usuario_documento 
   where usuario_id = ID_USUARIO;
   
   insert into usuario_documento (num_documento, usuario_id, cod_documento, num_tipo_documento)
   values (NUM_USUARIO_DOCUMENTO, ID_USUARIO, DOCUMENTO_COD, TIPO_DOCUMENTO);
end$ 
 --
DELIMITER $
create procedure spuUsuarioDocumento_Exclui(ID_USUARIO integer, DOCUMENTO_COD varchar(100))
begin  
   delete from usuario_documento 
   where usuario_id = ID_USUARIO
   and cod_documento = DOCUMENTO_COD;
end$ 
--
create table usuario_municipio
   (usuario_id integer,
    num_municipio integer,
    primary key (usuario_id, num_municipio),
    FOREIGN KEY (usuario_id) REFERENCES usuario(id) ON DELETE CASCADE,
    FOREIGN KEY (num_municipio) REFERENCES municipio(num_municipio) ON DELETE CASCADE)
--
DELIMITER $
create procedure spuUsuarioMunicipio_ListaPorUsuario(ID_USUARIO int)
begin
   select 	usuario_id as NumUsuario,
			num_municipio as NumMunicipio
   from usuario_municipio
   where usuario_id = ID_USUARIO;
end$
--
DELIMITER $
create procedure spuUsuarioMunicipio_Insere(ID_USUARIO int, ID_MUNICIPIO int)
begin
   insert into usuario_municipio (usuario_id, num_municipio)
   values (ID_USUARIO, ID_MUNICIPIO);
end$
--
DELIMITER $
create procedure spuUsuarioMunicipio_Exclui(ID_USUARIO int, ID_MUNICIPIO int)
begin
   delete from usuario_municipio 
   where usuario_id = ID_USUARIO
   and num_municipio = ID_MUNICIPIO;
end$
--
create table usuario_perfil_acesso
   (usuario_id integer not null,
	perfil_id integer not null,
    primary key (usuario_id, perfil_id),
    foreign key (usuario_id) references usuario(id),
    foreign key (perfil_id) references perfil_acesso(num_perfil))
--
DELIMITER $
create procedure spuUsuarioPerfil_ListaPeloUsuarioId(ID_USUARIO integer)
begin
    select 	usuario_id as NumUsuario,
			perfil_id as NumPerfil
    from usuario_perfil_acesso
    where usuario_id = ID_USUARIO;
end$ 