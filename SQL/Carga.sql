insert into parametro_sistema (cod_parametro, desc_parametro, val_parametro)
values ('UrlApiIBGEEstado', 'ULR de listagem dos estados', 'https://servicodados.ibge.gov.br/api/v1/localidades/estados');

insert into parametro_sistema (cod_parametro, desc_parametro, val_parametro)
values ('UrlApiIBGEMunicipioPorEstado', 'ULR de listagem dos municipios', 'https://servicodados.ibge.gov.br/api/v1/localidades/estados/{0}/municipios');

insert into perfil_acesso (desc_perfil, idc_admin)
values ('Administrador Aplicação', 'S');

insert into perfil_acesso (desc_perfil, idc_admin)
values ('Convidado', 'N');

insert into questionario (titulo_questionario)
values ('Farmacovigilância');

insert into tipo_formulario (desc_tipo_formulario)
values ('Banner Principal');

insert into tipo_formulario (desc_tipo_formulario)
values ('Conhecer a Hypofarma');

insert into tipo_formulario (desc_tipo_formulario)
values ('EnderecoRodape');

insert into tipo_formulario (desc_tipo_formulario)
values ('QuemSomos');

insert into tipo_formulario (desc_tipo_formulario)
values ('VideoSection');

insert into tipo_formulario (desc_tipo_formulario)
values ('AreaAtuacao');




-- Cadastros que devem ser feitos pela base de dados
Parâmetro
Perfil Acesso
Usuário x Perfil
Questionario
Tipo Formulario


