create table tipo_formulario
(num_tipo_formulario int not null AUTO_INCREMENT,
 desc_tipo_formulario varchar(100),
 primary key (num_tipo_formulario))
 --
DELIMITER $
create procedure spuTipoFormulario_ListaTodos()
begin
   select   num_tipo_formulario as Id,
			desc_tipo_formulario as DescTipoFormulario
   from tipo_formulario;
end$
--
 
 create table formulario
 ( num_formulario int not null AUTO_INCREMENT,
   num_tipo_formulario int,
   cont_formulario LONGTEXT,
   idc_ativo char(1),
   primary key (num_formulario),
   foreign key (num_tipo_formulario) references tipo_formulario(num_tipo_formulario))
--
DELIMITER $
create procedure spuFormulario_ListaPeloTipo(ID_TIPO int)
begin
   select   num_formulario as Id,
			num_tipo_formulario as NumTipoFormulario,
            cont_formulario as ContFormulario,
            case idc_ativo when 'S' then 1 else 0 end as IdcSituacao
   from formulario
   where num_tipo_formulario = ID_TIPO;
end$
--
DELIMITER $
create procedure spu_Formulario_Insere(ID_TIPO int, CONTEUDO LONGTEXT)
begin
   delete from formulario where num_tipo_formulario = ID_TIPO;
   
   insert into formulario (num_tipo_formulario, cont_formulario, idc_ativo)
   values (ID_TIPO, CONTEUDO, 'S');
end$
--
create table formulario_arquivo
  (num_arquivo int not null,
   num_tipo_formulario int not null,
   nom_arquivo varchar(100),
   cont_arquivo MEDIUMBLOB,
   primary key (num_arquivo, num_tipo_formulario),
   foreign key (num_tipo_formulario) references tipo_formulario(num_tipo_formulario))
--
DELIMITER $
create procedure spuFormularioArquivo_ListaPeloTipo(ID_TIPO int)
begin
   select 	num_arquivo as Id,
			num_tipo_formulario as NumTipoFormulario,
            nom_arquivo as NomArquivo,
            cont_arquivo as ContArquivo
	from formulario_arquivo
    where num_tipo_formulario = ID_TIPO;
end$
--
DELIMITER $
create procedure spuFormularioArquivo_Insere(ID_TIPO int, NOME_ARQUIVO varchar(100), CONTEUDO MEDIUMBLOB)
begin

   declare NUM_ARQUIVO_IMG int;   
   select count(num_arquivo) + 1 
   into NUM_ARQUIVO_IMG 
   from formulario_arquivo
   where num_tipo_formulario = ID_TIPO;
   
   insert into formulario_arquivo (num_arquivo, num_tipo_formulario, nom_arquivo, cont_arquivo)
   values (NUM_ARQUIVO_IMG, ID_TIPO, NOME_ARQUIVO, CONTEUDO);
end$
--
DELIMITER $
create procedure spuFormularioArquivo_Exclui(ID_TIPO int, NOME_ARQUIVO varchar(100))
begin
   delete from formulario_arquivo 
   where num_tipo_formulario = ID_TIPO
   and nom_arquivo = NOME_ARQUIVO;
end$
