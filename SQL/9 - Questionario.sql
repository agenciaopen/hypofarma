use dbHypofarma


--
create table questionario
  (num_questionario int not null AUTO_INCREMENT,
   titulo_questionario varchar(100),
   desc_questionario varchar(1000),
   detalhe_questionario varchar(1000),
   primary key (num_questionario))
--
DELIMITER $
create procedure spuQuestionario_ListaTodos()
begin
    select  num_questionario as Id,
            titulo_questionario as TituloQuestionario,
            desc_questionario as DescQuestionario,
            detalhe_questionario as DetalheQuestionario
    from questionario;
end$ 
--
create table resposta_questionario
  (num_resposta int not null AUTO_INCREMENT,
   num_questionario integer,
   dth_cadastro timestamp,
   desc_resposta LONGTEXT,
   primary key (num_resposta),
   foreign key (num_questionario) references questionario(num_questionario))
--
DELIMITER $
create procedure spuRespostaQuestionario_ListaPorDataCadastro( DT_CADASTRO timestamp)
begin
    select 	num_resposta as Id,
			num_questionario as NumQuestionario,
            dth_cadastro as DthCadastro,
			desc_resposta as DescResposta
    from resposta_questionario
    where day(dth_cadastro) = day(DT_CADASTRO)
    and month(dth_cadastro) = month(DT_CADASTRO)
    and year(dth_cadastro) = year(DT_CADASTRO);
end$
--
DELIMITER $
create procedure spuRespostaQuestionario_Insere(ID_QUESTIONARIO int, DT_CADASTRO timestamp, RESPOSTA LONGTEXT )
begin
   insert into resposta_questionario (num_questionario, dth_cadastro, desc_resposta)
   values (ID_QUESTIONARIO, DT_CADASTRO, RESPOSTA);
end$