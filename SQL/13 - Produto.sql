use dbHypofarma
--
create table produto
   (num_produto int not null AUTO_INCREMENT,
    nom_produto varchar(100),
    desc_concentracao varchar(50),
    desc_apresentacao varchar(500),
    cod_registro varchar(50),
    img_background MEDIUMBLOB,
    desc_indicacao varchar(500),
    idc_generico char(1),
    idc_exbibe_vitrine char(1),    
    idc_ativo char(1),
    num_categoria_regulatoria int,
    num_classe_terapeutica int,
    num_embalagem int,    
    num_principio_ativo int,    
    num_usuario_cadastro int,
    primary key (num_produto),
    check (idc_ativo = 'A' or idc_ativo = 'I'),
    check (idc_generico = 'S' or idc_generico = 'N'),
    check (idc_exbibe_vitrine = 'S' or idc_exbibe_vitrine = 'N'),
    foreign key (num_categoria_regulatoria) references categoria_regulatoria (num_categoria_regulatoria),
    foreign key (num_classe_terapeutica) references classe_terapeutica (num_classe_terapeutica),
    foreign key (num_embalagem) references embalagem (num_embalagem),
    foreign key (num_principio_ativo) references principio_ativo (num_principio_ativo),
    foreign key (num_usuario_cadastro) references usuario(id))
--
DELIMITER $
create procedure spuProduto_ListaFiltro(ID_PRODUTO int, ID_CATEGORIA int, ID_CLASSE int, ID_EMBALAGEM int, ID_PRINCIPIO_ATIVO int)
begin
  select 	num_produto as Id,
			nom_produto as NomProduto,
			desc_concentracao as Concentracao,
			desc_apresentacao as Apresentacao,
			cod_registro as Registro,
			img_background as ImgBackGround,
			desc_indicacao as Indicacao,
			case idc_generico when 'S' then 1 else 0 end as IdcGenerico,
			case idc_exbibe_vitrine when 'S' then 1 else 0 end  as IdcExibeVitrine,    
            case idc_ativo when 'A' then 'Ativo' else 'Inativo' end as IdcSituacao,
			num_categoria_regulatoria as NumCategoriaRegulatoria,
			num_classe_terapeutica as NumClasseTerapeutica,
			num_embalagem as NumEmbalagem,
			num_principio_ativo as NumPrincipioAtivo,
			num_usuario_cadastro as NumUsuarioCadastro
   from produto
   where ((num_produto = ID_PRODUTO) or (ID_PRODUTO = 0))
   and   ((num_categoria_regulatoria = ID_CATEGORIA) or (ID_CATEGORIA = 0))
   and 	 ((num_classe_terapeutica = ID_CLASSE) or (ID_CLASSE = 0))
   and   ((num_embalagem = ID_EMBALAGEM) or (ID_EMBALAGEM = 0))
   and   ((num_principio_ativo = ID_PRINCIPIO_ATIVO) or (ID_PRINCIPIO_ATIVO = 0));
end$
--
DELIMITER $
create procedure spuProduto_Insere(NOME varchar(100), CONCENTRACAO varchar(50), APRESENTACAO varchar(500), REGISTRO varchar(50), IMG_BACK MEDIUMBLOB, INDICACAO varchar(500), EH_GENERICO CHAR(1), EXIBE_VITRINE CHAR(1), ID_CATEGORIA int, ID_CLASSE int, ID_EMBALAGEM int, ID_PRINCIPIO_ATIVO int, ID_USUARIO_CADASTRO int)
begin
   insert into produto (nom_produto, desc_concentracao, desc_apresentacao, cod_registro, img_background, desc_indicacao, idc_generico, idc_exbibe_vitrine, idc_ativo, num_categoria_regulatoria, num_classe_terapeutica, num_embalagem, num_principio_ativo, num_usuario_cadastro)
   values (ltrim(rtrim(NOME)), ltrim(rtrim(CONCENTRACAO)), ltrim(rtrim(APRESENTACAO)), ltrim(rtrim(REGISTRO)), IMG_BACK, ltrim(rtrim(INDICACAO)), EH_GENERICO, EXIBE_VITRINE, 'A', ID_CATEGORIA, ID_CLASSE, ID_EMBALAGEM, ID_PRINCIPIO_ATIVO, ID_USUARIO_CADASTRO);
end$
--
DELIMITER $
create procedure spuProduto_Altera(ID_PRODUTO int, NOME varchar(100), CONCENTRACAO varchar(50), APRESENTACAO varchar(500), REGISTRO varchar(50), IMG_BACK MEDIUMBLOB, INDICACAO varchar(500), EH_GENERICO CHAR(1), EXIBE_VITRINE CHAR(1), ATIVO CHAR(1), ID_CATEGORIA int, ID_CLASSE int, ID_EMBALAGEM int, ID_PRINCIPIO_ATIVO int, ID_USUARIO_CADASTRO int)
begin
   update produto 
   set 	nom_produto = ltrim(rtrim(NOME)), 
		desc_concentracao = ltrim(rtrim(CONCENTRACAO)), 
        desc_apresentacao = ltrim(rtrim(APRESENTACAO)), 
        cod_registro = ltrim(rtrim(REGISTRO)), 
        img_background = IMG_BACK, 
        desc_indicacao = ltrim(rtrim(INDICACAO)), 
        idc_generico = EH_GENERICO, 
        idc_exbibe_vitrine = EXIBE_VITRINE, 
        idc_ativo = ATIVO, 
        num_categoria_regulatoria = ID_CATEGORIA, 
        num_classe_terapeutica = ID_CLASSE, 
        num_embalagem = ID_EMBALAGEM, 
        num_principio_ativo = ID_PRINCIPIO_ATIVO
   where num_produto = ID_PRODUTO;
end$
--
create table produto_documento
 ( num_produto integer not null,
   num_produto_documento integer not null,
   nom_arquivo varchar(100),
   arq_produto_documento MEDIUMBLOB,
   idc_escopo char(1),
   primary key (num_produto_documento, num_produto),
   foreign key (num_produto) references produto(num_produto),
   check (idc_escopo = 'M' or idc_escopo = 'P'))
--
DELIMITER $
create procedure spuProdutoDocumento_ListaPorProduto(ID_PRODUTO int)
begin
   select 	num_produto NumProduto,
			num_produto_documento as Id,
            nom_arquivo as NomArquivo,
            arq_produto_documento as ArqProduto,
            case idc_escopo when 'P' then 'Medico' else 'Paciente' end as IdcEscopo
	from produto_documento
    where num_produto = ID_PRODUTO;
end$
--
DELIMITER $
create procedure spuProdutoDocumento_Insere(ID_PRODUTO int, NOME_ARQUIVO varchar(100), ARQ_PRODUTO MEDIUMBLOB, ESCOPO char(1))
begin
   declare ID int;
   
   select count(num_produto) + 1 
   into ID 
   from produto_documento
   where num_produto = ID_PRODUTO;

   insert into produto_documento (num_produto, num_produto_documento, nom_arquivo, arq_produto_documento, idc_escopo)
   values (ID_PRODUTO, ID, NOME_ARQUIVO, ARQ_PRODUTO, ESCOPO);
end$
--
DELIMITER $
create procedure spuProdutoDocumento_Exclui(ID_DOCUMENTO int, ID_PRODUTO int)
begin
   delete from produto_documento 
   where num_produto = ID_PRODUTO
   and num_produto_documento = ID_DOCUMENTO;
end$