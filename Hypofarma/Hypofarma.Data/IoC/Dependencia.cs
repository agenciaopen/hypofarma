﻿using Hypofarma.Data.Data;
using Hypofarma.Data.Data.Formulario;
using Hypofarma.Data.Data.Interfaces;
using Hypofarma.Data.Data.Interfaces.Formulario;
using Hypofarma.Data.Data.Interfaces.Noticia;
using Hypofarma.Data.Data.Interfaces.Produto;
using Hypofarma.Data.Data.Interfaces.Questionario;
using Hypofarma.Data.Data.Interfaces.Rodape;
using Hypofarma.Data.Data.Interfaces.Usuario;
using Hypofarma.Data.Data.Noticia;
using Hypofarma.Data.Data.Produto;
using Hypofarma.Data.Data.Questionario;
using Hypofarma.Data.Data.Usuario;
using Hypofarma.Data.DataAccess;
using Microsoft.Extensions.DependencyInjection;

namespace Hypofarma.Data.IoC
{
    public static class Dependencia
    {
        public static void RegistrarDependencias(IServiceCollection servico)
        {
            servico.AddSingleton<ISqlDataAccess, SqlDataAccess>();

            #region Formulario

            servico.AddSingleton<IFormuarioDataService, FormuarioDataService>();

            servico.AddSingleton<ITipoFormularioDataService, TipoFormularioDataService>();

            servico.AddSingleton<IFormularioArquivoDataService, FormularioArquivoDataService>();

            #endregion

            #region Noticia

            servico.AddSingleton<INoticiaDataService, NoticiaDataService>();

            servico.AddSingleton<INoticiaImgDataService, NoticiaImgDataService>();

            #endregion

            #region Produto

            servico.AddSingleton<ICategoriaRegulatoriaDataService, CategoriaRegulatoriaDataService>();

            servico.AddSingleton<IClasseTerapeuticaDataService, ClasseTerapeuticaDataService>();

            servico.AddSingleton<IEmbalagemDataService, EmbalagemDataService>();

            servico.AddSingleton<IPrincipioAtivoDataService, PrincipioAtivoDataService>();

            servico.AddSingleton<IProdutoDataService, ProdutoDataService>();

            servico.AddSingleton<IProdutoDocumentoDataService, ProdutoDocumentoDataService>();

            #endregion

            #region Questionario

            servico.AddSingleton<IQuestionarioDataService, QuestionarioDataService>();

            servico.AddSingleton<IRespostaQuestionarioDataService, RespostaQuestionarioDataService>();

            #endregion

            #region Rodape

            servico.AddSingleton<IRodapeDataService, RodapeDataService>();

            #endregion

            #region Usuário

            servico.AddSingleton<IUsuarioDataService, UsuarioDataService>();

            servico.AddSingleton<IUsuarioDocumentoDataService, UsuarioDocumentoDataService>();

            servico.AddSingleton<IUsuarioLogDataService, UsuarioLogDataService>();

            servico.AddSingleton<IUsuarioMunicipioDataService, UsuarioMunicipioDataService>();

            servico.AddSingleton<IUsuarioPerfilDataService, UsuarioPerfilDataService>();

            servico.AddSingleton<IUsuarioTelefoneDataService, UsuarioTelefoneDataService>();

            #endregion

            servico.AddSingleton<ICertificadoAnaliseDataService, CertificadoAnaliseDataService>();

            servico.AddSingleton<IEstadoDataService, EstadoDataService>();

            servico.AddSingleton<IMunicipioDataService, MunicipioDataService>();

            servico.AddSingleton<IParametroSistemaService, ParametroSistemaService>();

            servico.AddSingleton<IPerfilAcessoDataService, PerfilAcessoDataService>();

        }
    }
}
