﻿

namespace Hypofarma.Data.Enum
{
    public enum MultiplaOpacaoRespostaEnum
    {
        Sim = 1,
        Nao = 2,
        NA = 3
    }
}
