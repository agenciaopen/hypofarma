﻿

namespace Hypofarma.Data.Enum
{
    public enum TipoDocumentoEnum
    {
        CPF = 1,
        CNPJ = 2
    }
}
