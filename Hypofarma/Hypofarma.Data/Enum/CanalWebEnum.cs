﻿
namespace Hypofarma.Data.Enum
{
    public enum CanalWebEnum
    {
        Instagram = 1,
        Facebook = 2,
        LinkedIn = 3,
        Twitter = 4,
        Youtube = 5
    }
}
