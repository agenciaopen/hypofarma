﻿using Hypofarma.Data.Enum;
using System.ComponentModel.DataAnnotations;

namespace Hypofarma.Data.Models.Rodape
{
    public sealed class MidiaWebModel: BaseModel
    {
        public CanalWebEnum CanalWeb { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Link do Perfil seja preenchido!")]
        public string LinkPerfil { get; set; }
    }
}
