﻿using System.Collections.Generic;

namespace Hypofarma.Data.Models.Rodape
{
    public sealed class RodaPeModel
    {
        public List<MidiaWebModel> MidiasWeb { get; set; }

    }
}
