﻿
namespace Hypofarma.Data.Models
{
    public class MunicipioModel
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public int NumEstado { get; set; }
    }
}
