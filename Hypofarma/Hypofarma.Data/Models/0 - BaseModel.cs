﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Hypofarma.Data.Models
{
    public abstract class BaseModel
    {
        public void Validate()
        {
            var _resultado = new List<ValidationResult>();

            if (!Validator.TryValidateObject(this, new ValidationContext(this, serviceProvider: null, items: null), _resultado, true))
            {
                var _lstErros = new StringBuilder();

                foreach (var item in _resultado)
                {
                    _lstErros.AppendLine( item.ErrorMessage );
                }

                throw new ValidationException( _lstErros.ToString() );
            }
        }
    }
}
