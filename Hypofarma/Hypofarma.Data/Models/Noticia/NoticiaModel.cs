﻿using Hypofarma.Data.Enum;
using System;
using System.ComponentModel.DataAnnotations;

namespace Hypofarma.Data.Models.Noticia
{
    public sealed class NoticiaModel : BaseModel
    {
        public NoticiaModel() => this.IdcSituacao = SituacaoEnum.Ativo;

        public int Id { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Título seja preenchido!")]
        [StringLength(256, MinimumLength = 5, ErrorMessage = "O Título deve possuir entre 5 e 256 caracteres")]
        public string TituloNoticia { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Descrição seja preenchido!")]
        [StringLength(8000, MinimumLength = 5, ErrorMessage = "A Descrição deve possuir entre 5 e 8000 caracteres")]
        public string DescNoticia { get; set; }

        public SituacaoEnum IdcSituacao { get; set; }

        public DateTime DthCadastro { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Usuário Cadastro seja preenchido!")]
        public int NumUsuarioCadastro { get; set; }
    }
}
