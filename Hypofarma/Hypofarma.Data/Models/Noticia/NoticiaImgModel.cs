﻿using Open.CORE.Validacao;
using System.ComponentModel.DataAnnotations;

namespace Hypofarma.Data.Models.Noticia
{
    public sealed class NoticiaImgModel : BaseModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Id da Notícia seja preenchido!")]
        public int NumNoticia { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Nome do Arquivo seja preenchido!")]
        [ValidaTipoArquivo(".jpg,.jpeg,.png")]
        public string NomArquivo { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Imagem seja preenchido!")]
        [ValidaTamanhoArquivo(500, "kb")]
        public byte[] ImgNoticia { get; set; }
    }
}
