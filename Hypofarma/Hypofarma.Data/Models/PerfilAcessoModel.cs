﻿
namespace Hypofarma.Data.Models
{
    public sealed class PerfilAcessoModel
    {
        public int Id { get; set; }

        public string DescPerfil { get; set; }

        public bool IdcAdmin { get; set; }
    }
}
