﻿
namespace Hypofarma.Data.Models
{
    public class EstadoModel
    {
        public int Id { get; set; }

        public string Sigla { get; set; }

        public string Nome  { get; set; }
    }
}
