﻿
namespace Hypofarma.Data.Models.Formulario
{
    public sealed class FormularioArquivoModel: BaseModel
    {
        public int Id { get; set; }

        public int NumTipoFormulario { get; set; }

        public string NomArquivo { get; set; }

        public string ContArquivo { get; set; }

    }

    public sealed class ArquivoDataView
    {
        public string NomArquivo { get; set; }

        public string ContArquivo { get; set; }
    }
}
