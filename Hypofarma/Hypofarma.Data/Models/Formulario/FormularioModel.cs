﻿using Hypofarma.Data.Enum;

namespace Hypofarma.Data.Models.Formulario
{
    public abstract class BaseFormulario
    {
        public int Id { get; set; }

        public string ContFormulario { get; set; }

        public SituacaoEnum IdcSituacao { get; set; }
    }

    public sealed class FormularioModel: BaseFormulario
    {     
        public int NumTipoFormulario { get; set; }

    }

    public sealed class FormularioViewModel : BaseFormulario
    {
        public TipoFormularioModel TipoFormulario { get; set; }

    }
}
