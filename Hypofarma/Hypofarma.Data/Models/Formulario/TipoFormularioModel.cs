﻿
namespace Hypofarma.Data.Models.Formulario
{
    public sealed class TipoFormularioModel
    {
        public int Id { get; set; }

        public string DescTipoFormulario { get; set; }
    }
}
