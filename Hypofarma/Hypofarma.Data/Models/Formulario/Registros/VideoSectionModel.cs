﻿
namespace Hypofarma.Data.Models.Formulario
{
    public sealed class VideoSectionModel
    {
        public string LnkVideo { get; set; }

        public byte[] ImgBackGround { get; set; }
    }
}
