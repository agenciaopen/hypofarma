﻿using System.Collections.Generic;

namespace Hypofarma.Data.Models.Formulario
{
    public sealed class AreaAtuacaoModel
    {
        public string TextoBotao { get; set; }

        public string LnkBotao { get; set; }

        public bool IdcOcultaBotao { get; set; }

        public List<FormularioArquivoModel> Arquivos { get; set; }
    }
}
