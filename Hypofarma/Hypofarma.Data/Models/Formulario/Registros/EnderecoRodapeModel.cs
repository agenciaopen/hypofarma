﻿
namespace Hypofarma.Data.Models.Formulario
{
    public sealed class EnderecoRodapeModel
    {
        public string Endereco1 { get; set; }

        public string Endereco2 { get; set; }
    }
}
