﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Hypofarma.Data.Models.Formulario
{
    public sealed class ConhecerAHypofarmaModel: BaseModel
    {
        [Required(ErrorMessage = "A aplicação requer que o campo Texto de Apresentação seja preenchido!")]
        public string TextApresentacao { get; set; }

        public List<FormularioArquivoModel> Arquivos { get; set; }
    }
}
