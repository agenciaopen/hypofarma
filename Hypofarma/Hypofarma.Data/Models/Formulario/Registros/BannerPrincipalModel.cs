﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Hypofarma.Data.Models.Formulario
{
    public sealed class BannerPrincipalModel: BaseModel
    {
        [Required(ErrorMessage = "A aplicação requer que o campo Título seja preenchido!")]
        public string Titulo { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Texto do Botão seja preenchido!")]
        public string TextoBotao { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Link do botão seja preenchido!")]
        public  string LinkBotao { get; set; }

        public List<FormularioArquivoModel> Arquivos { get; set; }
    }
}
