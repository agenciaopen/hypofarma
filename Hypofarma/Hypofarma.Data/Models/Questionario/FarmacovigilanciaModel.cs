﻿using Hypofarma.Data.Enum;
using System;

namespace Hypofarma.Data.Models.Questionario
{
    public sealed class FarmacovigilanciaModel
    {
        public string NomNotificador { get; set; }

        public bool IdcProfissionalSaude { get; set; }

        public string DesAreaProfissionalSaude { get; set; }

        public string NomInstituicao { get; set; }

        public bool IdcHospitalSentinelaFarmaciaNotificadora { get; set; }

        public bool IdcNotificouOrgaoRegularmente { get; set; }

        public string CodProtocoloNotificacaoOrgao { get; set; }


        public string NomPaciente { get; set; }

        public int NumCidade { get; set; }

        public string IdcSexo { get; set; }

        public bool IdcGestante { get; set; }

        public int QteSemanaGestante { get; set; }


        public string NomMedicamento { get; set; }

        public string CodLote { get; set; }

        public string NomProfissionalPrescritor { get; set; }

        public string NomENumeroConselho { get; set; }

        public bool IdcAmpolaAutoclaveAntesUso { get; set; }

        public string TempoETemperaturaAmpolaAutoclaveAntesUso { get; set; }

        public string DescIndicacao { get; set; }

        public string DescPosologiaDiluicaoUtilizada { get; set; }

        public string DescLocalInjecao { get; set; }

        public string DescCalibreAgulha { get; set; }

        public string DescPosicaoPacienteMomentoAplicacao { get; set; }

        public DateTime DtInicioTratamento { get; set; }

        public DateTime DtFimTratamento { get; set; }

        public int TempArmazenamentoGrausCelcius { get; set; }

        public int QteUmidade { get; set; }

        public bool IdcProtecaoLuz { get; set; }

        public bool IdcAlteracaoOrganolepticaMedicamento { get; set; }


        public bool IdcUsaOutroMedicamento { get; set; }
        public string DescMedicamentoUsado { get; set; }

        public bool IdcMedicamentoUsoContinuo { get; set; }

        public string DescTempoUsoMedicamentoUsoContinuo { get; set; }

        public bool IdcHistoricoDoencaCronica { get; set; }

        public string DescHistoricoDoencaCronica { get; set; }

        public bool IdcAlergia { get; set; }

        public string DescAlergia { get; set; }

        public bool IdcFumante { get; set; }

        public bool IdcFazUsoBebidaAlcoolica { get; set; }

        public bool IdcPassouPorCirurgiaRecentemente { get; set; }

        public string DescTempoPassouPorCirurgiaRecentemente { get; set; }

        public string QualCirurgiaPassouRecentemente { get; set; }

        public string DescInformacaoComplementarUsaOutroMedicamento { get; set; }

        
        public bool IdcPacienteHospitalizadoMomentoOcorrencia { get; set; }

        public string DescMotivoPacienteHospitalizadoMomentoOcorrencia { get; set; }

        public bool IdcNecessitouInternacao { get; set; }

        public bool IdcInternacaoSemSequela { get; set; }

        public bool IdcProlongouInternacao { get; set; }

        public bool IdcObito { get; set; }

        public bool IdcMudancaMedicamento { get; set; }

        public string DescAlteracaoPosologiaViaAdminMedicamentoSuspeito { get; set; }
        
        public bool IdcEventoAdversoGrave { get; set; }

        public MultiplaOpacaoRespostaEnum IdcHouveMelhoraAposTratamentoInterrompido { get; set; }

        public MultiplaOpacaoRespostaEnum IdcEventoAdversoReapareceuComNovaAdminMedicamento { get; set; }

        public MultiplaOpacaoRespostaEnum IdcEventoAumentouComDoseMaiorOuReduziuComMenor { get; set; }

        public MultiplaOpacaoRespostaEnum UtilizacoAlgumOutroMomentoParaTratamentoSintomaEventoAdverso { get; set; }

        public string DescMomentoParaTratamentoSintomaEventoAdverso { get; set; }

        public MultiplaOpacaoRespostaEnum IdcEventoReapareceuInducaoPlacebo { get; set; }

        public MultiplaOpacaoRespostaEnum IdcFarmacoDetectadoNoSangueOuFluidosConcepcaoToxica { get; set; }

        public MultiplaOpacaoRespostaEnum IdcPacienteTemHistoricoSemelhanteComMesmoFarmacoPosicaoPrevia { get; set; }


        public bool IdcReacaoAdversa { get; set; }
        public DateTime DtInicioReacaoAdversa { get; set; }
        public DateTime DtFimReacaoAdversa { get; set; }

        public string DescTempoAdminProdutoEAparecimentoEvento { get; set; }
        public string DescRelatoReacaoAdversaPrincipaisSintomas { get; set; }

        
        public bool IdcInefetividadeTerapeutica { get; set; }

        public bool IdcIneficaciaTotal { get; set; }

        public string DescInformacaoComplementarInefetividadeTerapeutica { get; set; }


        public bool IdcInteracaoMedicamento { get; set; }

        public string DescEfeitoPotencialInteracaoMedicamento { get; set; }

        public string DescInformacaoComplementarInteracaoMedicamento { get; set; }


        public string DescComentario { get; set; }

        public string NomResponsavel { get; set; }

        public DateTime DtComentario { get; set; }

    }
}
