﻿namespace Hypofarma.Data.Models.Questionario
{
    public sealed class QuestionarioModel
    {
        public int Id { get; set; }

        public string TituloQuestionario { get; set; }

        public string DescQuestionario { get; set; }

        public string DetalheQuestionario { get; set; }
    }
}
