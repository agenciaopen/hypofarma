﻿using System;

namespace Hypofarma.Data.Models.Questionario
{
    public sealed class RespostaQuestionarioModel
    {
        public int Id { get; set; }

        public string DescResposta { get; set; }

        public DateTime DthCadastro { get; set; }

        public int NumQuestionario { get; set; }
    }
}
