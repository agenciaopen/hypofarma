﻿using System.ComponentModel.DataAnnotations;

namespace Hypofarma.Data.Models
{
    public sealed class UsuarioMunicipioModel: BaseModel
    {
        [Required(ErrorMessage = "A aplicação requer que o campo Id Usuário seja preenchido!")]
        public int NumUsuario { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Id Município seja preenchido!")]
        public int NumMunicipio { get; set; }
    }
}
