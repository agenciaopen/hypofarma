﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Hypofarma.Data.Models
{
    public sealed class UsuarioLogModel: BaseModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Id Usuário seja preenchido!")]
        public int NumUsuario { get; set; }

        public DateTime DataLog { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Descrição seja preenchido!")]
        [StringLength(500, ErrorMessage = "O E-Mail deve possuir no máximo 500 caracteres")]
        public string DescLog { get; set; }

    }
}
