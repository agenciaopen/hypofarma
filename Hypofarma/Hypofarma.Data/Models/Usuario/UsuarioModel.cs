﻿using Hypofarma.Data.Enum;
using Open.CORE.Validacao;
using System.ComponentModel.DataAnnotations;

namespace Hypofarma.Data.Models
{
    public sealed class UsuarioModel: BaseModel
    {
        public UsuarioModel() => this.IdcSituacao = SituacaoEnum.Ativo;

        public int Id { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Nome seja preenchido!")]
        [StringLength(100, MinimumLength = 5, ErrorMessage = "O Nome deve possuir entre 5 e 100 caracteres")]
        public string NomUsuario { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Senha seja preenchido!")]
        [StringLength(10, MinimumLength = 5, ErrorMessage = "A Senha deve possuir entre 5 e 100 caracteres")]
        public string DesSenha { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo E-Mail seja preenchido!")]
        [StringLength(100, MinimumLength = 10, ErrorMessage = "O E-Mail deve possuir entre 10 e 100 caracteres")]
        [ValidaEMail]
        public string EMail { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Situação seja preenchido!")]
        public SituacaoEnum IdcSituacao { get; set; }
    }
}
