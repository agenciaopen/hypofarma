﻿using Hypofarma.Data.Enum;
using System.ComponentModel.DataAnnotations;

namespace Hypofarma.Data.Models.Usuario
{
    public sealed class UsuarioDocumentoModel: BaseModel
    {
        public UsuarioDocumentoModel() => this.TipoDocumento = TipoDocumentoEnum.CPF;

        public int Id { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Id Usuário seja preenchido!")]
        public int NumUsuario { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Número do Documento seja preenchido!")]
        [StringLength(100, MinimumLength = 5, ErrorMessage = "O Número do Documento deve possuir entre 5 e 100 caracteres")]
        public string CodDocumento { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Tipo do Documento seja preenchido!")]        
        public TipoDocumentoEnum TipoDocumento { get; set; }
    }
}
