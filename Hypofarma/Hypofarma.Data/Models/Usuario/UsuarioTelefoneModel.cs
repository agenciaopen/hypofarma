﻿using Hypofarma.Data.Enum;
using System.ComponentModel.DataAnnotations;

namespace Hypofarma.Data.Models
{
    public sealed class UsuarioTelefoneModel: BaseModel
    {
        public UsuarioTelefoneModel() => this.TipoTelefone = TipoTelefoneEnum.Particular;

        public int Id { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Id Usuário seja preenchido!")]
        public int NumUsuario { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Tipo Telefone seja preenchido!")]
        public TipoTelefoneEnum TipoTelefone { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Telefone seja preenchido!")]
        [StringLength(11, MinimumLength = 10, ErrorMessage = "O E-Mail deve possuir entre 10 e 11 caracteres")]
        public string NumTelefone { get; set; }

        public bool IdcWhatsApp { get; set; }
    }
}
