﻿
namespace Hypofarma.Data.Models
{
    public sealed class UsuarioPerfilModel
    {
        public int NumUsuario { get; set; }

        public int NumPerfil { get; set; }
    }
}
