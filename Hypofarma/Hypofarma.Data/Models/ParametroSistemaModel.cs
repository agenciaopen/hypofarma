﻿
namespace Hypofarma.Data.Models
{
    public sealed class ParametroSistemaModel
    {
        public string CodParametro { get; set; }
        public string DescParametro { get; set; }
        public string ValParametro { get; set; }
    }
}
