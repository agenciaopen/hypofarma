﻿using Hypofarma.Data.Enum;
using Open.CORE.Validacao;
using System.ComponentModel.DataAnnotations;

namespace Hypofarma.Data.Models.Produto
{
    public sealed class ProdutoDocumentoModel : BaseModel
    {
        public ProdutoDocumentoModel() => this.IdcEscopo = EscopoArquivoProdutoEnum.Medico;

        [Required(ErrorMessage = "A aplicação requer que o campo Num Produto seja preenchido!")]
        public int NumProduto { get; set; }

        public int Id { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Nome do Arquivo seja preenchido!")]
        [StringLength(100, MinimumLength = 10, ErrorMessage = "O Nome do Arquivo deve possuir entre 10 e 100 caracteres")]
        [ValidaTipoArquivo(".pdf")]
        public string NomArquivo { get; set; }

        [ValidaTamanhoArquivo(2, "MB")]
        public byte[] ArqProduto { get; set; }

        public EscopoArquivoProdutoEnum IdcEscopo { get; set; }

    }
}
