﻿using Hypofarma.Data.Enum;
using Open.CORE.Validacao;
using System.ComponentModel.DataAnnotations;

namespace Hypofarma.Data.Models.Produto
{
    public abstract class BaseProduto: BaseModel
    {
        public BaseProduto() => this.IdcSituacao = SituacaoEnum.Ativo;

        public int Id { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Descrição seja preenchido!")]
        [StringLength(100, MinimumLength = 5, ErrorMessage = "A Descrição deve possuir entre 5 e 100 caracteres")]
        public string NomProduto { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Concentração seja preenchido!")]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "A Concentração deve possuir entre 5 e 50 caracteres")]
        public string Concentracao { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Apresentação seja preenchido!")]
        [StringLength(500, MinimumLength = 5, ErrorMessage = "A Apresentação deve possuir entre 5 e 500 caracteres")]
        public string Apresentacao { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Registro seja preenchido!")]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "O Registro deve possuir entre 5 e 50 caracteres")]
        public string Registro { get; set; }

        [ValidaTamanhoArquivo(500, "kb")]
        public byte[] ImgBackGround { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Indicação seja preenchido!")]
        [StringLength(500, MinimumLength = 5, ErrorMessage = "A Indicação deve possuir entre 5 e 500 caracteres")]
        public string Indicacao { get; set; }

        public bool IdcGenerico { get; set; }

        public bool IdcExibeVitrine { get; set; }

        public SituacaoEnum IdcSituacao { get; set; }
    }

    public sealed class ProdutoModel : BaseProduto
    {
        [Required(ErrorMessage = "A aplicação requer que o campo Categoria Regulatória seja preenchido!")]
        public int NumCategoriaRegulatoria { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Classe Terapeutica seja preenchido!")]
        public int NumClasseTerapeutica { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Embalagem seja preenchido!")]
        public int NumEmbalagem { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Princípio Ativo seja preenchido!")]
        public int NumPrincipioAtivo { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Usuário Cadastro seja preenchido!")]
        public int NumUsuarioCadastro { get; set; }
    }

    public sealed class ProdutoViewModel : BaseProduto
    {
        public CategoriaRegulatoriaModel  CategoriaRegulatoria { get; set; }

        public ClasseTerapeuticaModel ClasseTerapeutica { get; set; }

        public EmbalagemModel Embalagem { get; set; }

        public PrincipioAtivoModel PrincipioAtivo { get; set; }

        public UsuarioModel UsuarioCadastro { get; set; }
    }
}
