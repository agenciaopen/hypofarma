﻿using Hypofarma.Data.Enum;
using Open.CORE.Validacao;
using System.ComponentModel.DataAnnotations;

namespace Hypofarma.Data.Models.Produto
{
    public sealed class EmbalagemModel : BaseModel
    {
        public EmbalagemModel() => this.IdcSituacao = SituacaoEnum.Ativo;

        public int Id { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Descrição seja preenchido!")]
        [StringLength(100, MinimumLength = 5, ErrorMessage = "A Descrição deve possuir entre 5 e 100 caracteres")]
        public string DescEmbalagem { get; set; }

        [ValidaTamanhoArquivo(500, "kb")]
        public byte[] ArqIconeEmbalagem { get; set; }

        public SituacaoEnum IdcSituacao { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Usuário Cadastro seja preenchido!")]
        public int NumUsuarioCadastro { get; set; }
    }
}
