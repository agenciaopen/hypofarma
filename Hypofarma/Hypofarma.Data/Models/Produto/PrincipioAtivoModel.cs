﻿using Hypofarma.Data.Enum;
using System.ComponentModel.DataAnnotations;

namespace Hypofarma.Data.Models.Produto
{
    public sealed class PrincipioAtivoModel: BaseModel
    {
        public PrincipioAtivoModel() => this.IdcSituacao = SituacaoEnum.Ativo;

        public int Id { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Descrição seja preenchido!")]
        [StringLength(100, MinimumLength = 5, ErrorMessage = "A Descrição deve possuir entre 5 e 100 caracteres")]
        public string DescPrincipioAtivo { get; set; }

        public SituacaoEnum IdcSituacao { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Usuário Cadastro seja preenchido!")]
        public int NumUsuarioCadastro { get; set; }
    }
}
