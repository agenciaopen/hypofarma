﻿using Open.CORE.Validacao;
using System.ComponentModel.DataAnnotations;

namespace Hypofarma.Data.Models
{
    public sealed class CertificadoAnaliseModel: BaseModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Nome do Arquivo seja preenchido!")]
        [ValidaTipoArquivo(".pdf")]
        public string NomArquCertificado { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Arquivo seja preenchido!")]
        [ValidaTamanhoArquivo(2, "MB")]
        public byte[] ArqCertificado { get; set; }

        [Required(ErrorMessage = "A aplicação requer que o campo Usuário Cadastro seja preenchido!")]
        public int NumUsuarioCadastro { get; set; }
    }
}
