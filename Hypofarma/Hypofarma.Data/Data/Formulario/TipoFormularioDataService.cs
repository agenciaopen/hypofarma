﻿using Hypofarma.Data.Data.Interfaces.Formulario;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models.Formulario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data.Formulario
{
    internal class TipoFormularioDataService: BaseDataService, ITipoFormularioDataService
    {
        public TipoFormularioDataService(ISqlDataAccess dataAccess)
            : base(dataAccess) { }

        public List<TipoFormularioModel> GetAll()
        {
            try
            {
                return _dataAccess.LoadDataSync<TipoFormularioModel, dynamic>("spuTipoFormulario_ListaTodos", new { }).ToList<TipoFormularioModel>();
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        public TipoFormularioModel GetById(int id)
        {
            return this.GetAll().Where(o => o.Id == id).FirstOrDefault();
        }
    }
}
