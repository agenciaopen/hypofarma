﻿using Hypofarma.Data.Data.Interfaces.Formulario;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models.Formulario;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data.Formulario
{
    internal sealed class FormularioArquivoDataService: BaseDataService, IFormularioArquivoDataService
    {
        public FormularioArquivoDataService(ISqlDataAccess dataAccess)
            : base(dataAccess) { }

        public List<FormularioArquivoModel> GetPeloTipo(int idTipo)
        {
            try
            {
                return _dataAccess.LoadDataSync<FormularioArquivoModel, dynamic>("spuFormularioArquivo_ListaPeloTipo", new { ID_TIPO = idTipo }).ToList<FormularioArquivoModel>();
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        public string Add(FormularioArquivoModel obj)
        {
            if (this.GetPeloTipo(obj.NumTipoFormulario).Any(x => x.NomArquivo.ToLower() == obj.NomArquivo.ToLower().Trim()))
            {
                return "Arquivo já cadastrado!";
            }

            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuFormularioArquivo_Insere", new { ID_TIPO = obj.NumTipoFormulario, NOME_ARQUIVO = obj.NomArquivo, CONTEUDO = obj.ContArquivo });
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public string Remove(FormularioArquivoModel obj)
        {
            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuFormularioArquivo_Exclui", new { ID_TIPO = obj.NumTipoFormulario, NOME_ARQUIVO = obj.NomArquivo});
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }
    }
}
