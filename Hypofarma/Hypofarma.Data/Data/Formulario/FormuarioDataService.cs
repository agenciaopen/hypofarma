﻿using Hypofarma.Data.Data.Interfaces.Formulario;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models.Formulario;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text.Json;

namespace Hypofarma.Data.Data.Formulario
{
    public sealed class FormuarioDataService: BaseDataService, IFormuarioDataService
    {
        readonly ITipoFormularioDataService _tipo;
        readonly IFormularioArquivoDataService _arquivo;

        public FormuarioDataService(ISqlDataAccess dataAccess, 
                                    ITipoFormularioDataService tipo,
                                    IFormularioArquivoDataService arquivo)
            : base(dataAccess) 
        {
            _tipo = tipo;
            _arquivo = arquivo;
        }

        T GetFormulario<T>(int idTipo)
        {
            var _obj =  _dataAccess.LoadDataSync<FormularioModel, dynamic>("spuFormulario_ListaPeloTipo", new { ID_TIPO = idTipo }).FirstOrDefault();

            return _obj == null ? (T)Activator.CreateInstance(typeof(T)): JsonSerializer.Deserialize<T>(_obj.ContFormulario);
        }

        string GravaFormulario(FormularioModel form)
        {
            try
            {
                _dataAccess.SaveData("spu_Formulario_Insere", new { ID_TIPO = form.NumTipoFormulario, CONTEUDO = form.ContFormulario });
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        string GravaArquivosFormulario(FormularioArquivoModel arqu)
        {

            try
            {
                _dataAccess.SaveData("spuFormularioArquivo_Insere", new { ID_TIPO = arqu.NumTipoFormulario, NOME_ARQUIVO = arqu.NomArquivo, CONTEUDO = arqu.ContArquivo });
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }


            return string.Empty;
        }

        List<FormularioArquivoModel> GetArquivos(int idTipo)
        {
            return _arquivo.GetPeloTipo(idTipo);
        }


        public BannerPrincipalModel GetBannerPrincipal()
        {
            var _idBanner = _tipo.GetAll().Where(x => x.DescTipoFormulario.ToLower() == "Banner Principal".ToLower()).FirstOrDefault().Id;

            var _resultado = this.GetFormulario<BannerPrincipalModel>(_idBanner);

            _resultado.Arquivos = this.GetArquivos(_idBanner);

            return _resultado;
        }

        public string GravaBannerPrincipal(BannerPrincipalModel banner, List<ArquivoDataView> arquivos)
        {
            var _idBanner = _tipo.GetAll().Where(x => x.DescTipoFormulario.ToLower() == "Banner Principal".ToLower()).FirstOrDefault().Id;

            try
            {
                banner.Validate();

                this.GravaFormulario(new FormularioModel() {
                                                             NumTipoFormulario = _idBanner,
                                                             ContFormulario = JsonSerializer.Serialize(banner)
                                                           });
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "Erro Ao gravar Formulário: " + ex.Message;
            }

            try
            {
                foreach (var item in arquivos)
                {
                    this.GravaArquivosFormulario( new FormularioArquivoModel() { NumTipoFormulario = _idBanner, NomArquivo = item.NomArquivo, ContArquivo = item.ContArquivo });
                }
            }
            catch (Exception ex)
            {
                return "Erro Ao gravar Arquivos Formulário: " + ex.Message;
            }

            return string.Empty;
        }


        public string GravaApresentacaoHypofarma(ConhecerAHypofarmaModel apresentacao, List<ArquivoDataView> arquivos)
        {
            var _idApresentacao = _tipo.GetAll().Where(x => x.DescTipoFormulario.ToLower() == "Conhecer a Hypofarma".ToLower()).FirstOrDefault().Id;

            try
            {
                apresentacao.Validate();

                this.GravaFormulario(new FormularioModel()
                {
                    NumTipoFormulario = _idApresentacao,
                    ContFormulario = JsonSerializer.Serialize(apresentacao)
                });
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "Erro Ao gravar Formulário: " + ex.Message;
            }

            try
            {
                foreach (var item in arquivos)
                {
                    this.GravaArquivosFormulario(new FormularioArquivoModel() { NumTipoFormulario = _idApresentacao, NomArquivo = item.NomArquivo, ContArquivo = item.ContArquivo });
                }
            }
            catch (Exception ex)
            {
                return "Erro Ao gravar Arquivos Formulário: " + ex.Message;
            }

            return string.Empty;
        }

        public ConhecerAHypofarmaModel GetApresentacao()
        {
            var _idApresentacao = _tipo.GetAll().Where(x => x.DescTipoFormulario.ToLower() == "Conhecer a Hypofarma".ToLower()).FirstOrDefault().Id;

            var _resultado = this.GetFormulario<ConhecerAHypofarmaModel>(_idApresentacao);

            _resultado.Arquivos = this.GetArquivos(_idApresentacao);

            return _resultado;
        }


        public string GravaEnderecoRodape(EnderecoRodapeModel rodape)
        {
            var _id = _tipo.GetAll().Where(x => x.DescTipoFormulario.ToLower() == "EnderecoRodape".ToLower()).FirstOrDefault().Id;

            try
            {
                this.GravaFormulario(new FormularioModel()
                {
                    NumTipoFormulario = _id,
                    ContFormulario = JsonSerializer.Serialize(rodape)
                });
            }
            catch (Exception ex)
            {
                return "Erro Ao gravar Formulário: " + ex.Message;
            }

            return string.Empty;
        }

        public EnderecoRodapeModel GetRodape()
        {
            return this.GetFormulario<EnderecoRodapeModel>(_tipo.GetAll().Where(x => x.DescTipoFormulario.ToLower() == "EnderecoRodape".ToLower()).FirstOrDefault().Id);
        }


        public string GravaQuemSomos(QuemSomosModel frm)
        {
            var _id = _tipo.GetAll().Where(x => x.DescTipoFormulario.ToLower() == "QuemSomos".ToLower()).FirstOrDefault().Id;

            try
            {
                this.GravaFormulario(new FormularioModel()
                {
                    NumTipoFormulario = _id,
                    ContFormulario = JsonSerializer.Serialize(frm)
                });
            }
            catch (Exception ex)
            {
                return "Erro Ao gravar Formulário: " + ex.Message;
            }

            return string.Empty;
        }

        public QuemSomosModel GetSomos()
        {
            return this.GetFormulario<QuemSomosModel>(_tipo.GetAll().Where(x => x.DescTipoFormulario.ToLower() == "QuemSomos".ToLower()).FirstOrDefault().Id);
        }


        public string GravaVideoSection(VideoSectionModel vs)
        {
            var _id = _tipo.GetAll().Where(x => x.DescTipoFormulario.ToLower() == "VideoSection".ToLower()).FirstOrDefault().Id;

            try
            {
                this.GravaFormulario(new FormularioModel()
                {
                    NumTipoFormulario = _id,
                    ContFormulario = JsonSerializer.Serialize(vs)
                });
            }
            catch (Exception ex)
            {
                return "Erro Ao gravar Formulário: " + ex.Message;
            }

            return string.Empty;
        }

        public VideoSectionModel GetVideoSection()
        {
            return this.GetFormulario<VideoSectionModel>(_tipo.GetAll().Where(x => x.DescTipoFormulario.ToLower() == "VideoSection".ToLower()).FirstOrDefault().Id);
        }


        public string GravaAreaAtuacao(AreaAtuacaoModel area, List<ArquivoDataView> arquivos)
        {
            var _idBanner = _tipo.GetAll().Where(x => x.DescTipoFormulario.ToLower() == "AreaAtuacao".ToLower()).FirstOrDefault().Id;

            try
            {
                this.GravaFormulario(new FormularioModel()
                {
                    NumTipoFormulario = _idBanner,
                    ContFormulario = JsonSerializer.Serialize(area)
                });
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "Erro Ao gravar Formulário: " + ex.Message;
            }

            try
            {
                foreach (var item in arquivos)
                {
                    this.GravaArquivosFormulario(new FormularioArquivoModel() { NumTipoFormulario = _idBanner, NomArquivo = item.NomArquivo, ContArquivo = item.ContArquivo });
                }
            }
            catch (Exception ex)
            {
                return "Erro Ao gravar Arquivos Formulário: " + ex.Message;
            }

            return string.Empty;
        }

        public AreaAtuacaoModel GetAreaAtuacao()
        {
            var _idAreaAtuacao = _tipo.GetAll().Where(x => x.DescTipoFormulario.ToLower() == "AreaAtuacao".ToLower()).FirstOrDefault().Id;

            var _resultado = this.GetFormulario<AreaAtuacaoModel>(_idAreaAtuacao);

            _resultado.Arquivos = this.GetArquivos(_idAreaAtuacao);

            return _resultado;
        }

    }
}
