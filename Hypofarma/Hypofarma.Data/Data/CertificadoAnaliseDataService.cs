﻿using Hypofarma.Data.Data.Interfaces;
using Hypofarma.Data.Data.Interfaces.Usuario;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data
{
    public sealed class CertificadoAnaliseDataService : BaseDataService, ICertificadoAnaliseDataService
    {
        readonly IUsuarioLogDataService _log;

        public CertificadoAnaliseDataService(ISqlDataAccess dataAccess, IUsuarioLogDataService log)
            : base(dataAccess) => _log = log;

        public List<CertificadoAnaliseModel> GetAll()
        {
            return base.GetAll<CertificadoAnaliseModel>("spuCertificadoAnalise_ListaTodos");
        }

        public CertificadoAnaliseModel GetById(int id)
        {
            try
            {
                return _dataAccess.LoadDataSync<CertificadoAnaliseModel, dynamic>("spuCertificadoAnalise_ListaPeloId", new { ID_CERTIFICADO = id }).FirstOrDefault(); ;
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        public List<CertificadoAnaliseModel> GetByNomeArquivo(string nomArquivo)
        {
            return this.GetAll().Where(o => o.NomArquCertificado.ToLower().Contains(nomArquivo.ToLower().Trim())).ToList();
        }

        CertificadoAnaliseModel BuscaPeloNome(string nomArquivo)
        {
            try
            {
                return _dataAccess.LoadDataSync<CertificadoAnaliseModel, dynamic>("spuCertificadoAnalise_ListaPeloNomArquivo", new { NOM_ARQUIVO = nomArquivo }).FirstOrDefault(); ;
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        public string Add(CertificadoAnaliseModel obj)
        {
            if (this.BuscaPeloNome(obj.NomArquCertificado) != null)
            {
                return "Arquivo ja cadastrado!";
            }

            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuCertificadoAnalise_Insere", new { NOM_ARQUIVO = obj.NomArquCertificado, ARQUIVO = obj.ArqCertificado, ID_USUARIO_CADASTRO = obj.NumUsuarioCadastro });
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public string Remove(CertificadoAnaliseModel obj)
        {
            return base.Remove("spuCertificadoAnalise_Exclui", obj.Id);
        }

        public string RegistraLogAcessoArquivo(CertificadoAnaliseModel arquivo, UsuarioModel usuario)
        {
            return _log.Add( new UsuarioLogModel() { NumUsuario = usuario.Id, DescLog = "Acesso ao Arquivo " + arquivo.NomArquCertificado });
        }
    }
}
