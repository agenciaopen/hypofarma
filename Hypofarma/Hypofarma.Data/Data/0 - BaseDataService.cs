﻿using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data
{
    public abstract class BaseDataService
    {
        protected readonly ISqlDataAccess _dataAccess;

        public BaseDataService(ISqlDataAccess dataAccess) => _dataAccess = dataAccess;

        protected List<T> GetAll<T>(string spName)
        {
            try
            {
                return _dataAccess.LoadDataSync<T, dynamic>(spName, new { }).ToList<T>();
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        protected string Remove(string spName, int id)
        {
            try
            {
                _dataAccess.SaveData(spName, new { ID = id });

            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        protected string GetSituacao(SituacaoEnum sit)
        {
            return sit == SituacaoEnum.Ativo ? "A" : "I";
        }
    }
}
