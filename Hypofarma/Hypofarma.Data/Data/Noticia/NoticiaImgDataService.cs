﻿using Hypofarma.Data.Data.Interfaces.Noticia;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models.Noticia;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data.Noticia
{
    internal sealed class NoticiaImgDataService: BaseDataService, INoticiaImgDataService
    {
        public NoticiaImgDataService(ISqlDataAccess dataAccess)
            : base(dataAccess) { }

        public List<NoticiaImgModel> BuscaPelaNoticia(int Id)
        {
            try
            {
                return _dataAccess.LoadDataSync<NoticiaImgModel, dynamic>("spuNoticiaImg_ListaPorNoticia", new { ID_NOTICIA = Id }).ToList<NoticiaImgModel>();
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        public string Add(NoticiaImgModel obj)
        {
            if (this.BuscaPelaNoticia(obj.NumNoticia).Any(x => x.NomArquivo.ToLower() == obj.NomArquivo.ToLower().Trim()))
            {
                return "Arquivo já cadastrado!";
            }

            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuNoticiaImg_Insere", new { ID_NOTICIA = obj.NumNoticia, NOME_ARQUIVO = obj.NomArquivo, IMG = obj.ImgNoticia});
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public string Remove(NoticiaImgModel obj)
        {
            try
            {
                _dataAccess.SaveData("spuNoticiaImg_Exclui", new { ID_NOTICIA = obj.NumNoticia, ID_ARQUIVO = obj.Id });
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }
    }
}
