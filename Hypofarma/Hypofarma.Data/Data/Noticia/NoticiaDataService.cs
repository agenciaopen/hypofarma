﻿using Hypofarma.Data.Data.Interfaces.Noticia;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models.Noticia;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data.Noticia
{
    public sealed class NoticiaDataService : BaseDataService, INoticiaDataService
    {
        readonly INoticiaImgDataService _img;

        public NoticiaDataService(ISqlDataAccess dataAccess, INoticiaImgDataService img)
            : base(dataAccess) => _img = img;

        public List<NoticiaModel> GetAll()
        {
            return base.GetAll<NoticiaModel>("spuNoticia_ListaTodos");
        }

        public string Add(NoticiaModel obj)
        {
            if (this.GetAll().Any(x => x.TituloNoticia.ToLower() == obj.TituloNoticia.ToLower().Trim()))
            {
                return "Já existe uma Notícia cadastrada com esse Título";
            }

            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuNoticia_Insere", new { TITULO = obj.TituloNoticia, DESCRICAO = obj.DescNoticia, DT_CADASTRO = DateTime.Now, ID_USUARIO_CADASTRO = obj.NumUsuarioCadastro } );
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public string Update(NoticiaModel obj)
        {
            if (this.GetAll().Any(x => x.TituloNoticia.ToLower() == obj.TituloNoticia.ToLower().Trim() && x.Id != obj.Id))
            {
                return "Já existe uma Notícia cadastrada com esse Título";
            }

            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuNoticia_Altera", new { ID_NOTICIA = obj.Id, TITULO = obj.TituloNoticia, DESCRICAO = obj.DescNoticia, ATIVO = GetSituacao(obj.IdcSituacao) });
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public string Remove(NoticiaModel obj)
        {
            return base.Remove("spuNoticia_Exclui", obj.Id);
        }


        public List<NoticiaImgModel> GetImg(int idNoticia)
        {
            return _img.BuscaPelaNoticia(idNoticia);
        }

        public string AddImg(NoticiaImgModel img)
        {
            return _img.Add(img);
        }

        public string RemoveImg(NoticiaImgModel img)
        {
            return _img.Remove(img);
        }
    }
}
