﻿using Hypofarma.Data.Data.Interfaces.Questionario;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models.Questionario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data.Questionario
{
    internal sealed class RespostaQuestionarioDataService : BaseDataService, IRespostaQuestionarioDataService
    {
        public RespostaQuestionarioDataService(ISqlDataAccess dataAccess)
            : base(dataAccess) { }

        public List<RespostaQuestionarioModel> GetByDataCadastro( DateTime dthCadastro)
        {
            try
            {
                return _dataAccess.LoadDataSync<RespostaQuestionarioModel, dynamic>("spuRespostaQuestionario_ListaPorDataCadastro", new { DT_CADASTRO = dthCadastro}).ToList<RespostaQuestionarioModel>();
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        public string Add(RespostaQuestionarioModel obj)
        {
            try
            {
                _dataAccess.SaveData("spuRespostaQuestionario_Insere", new { ID_QUESTIONARIO = obj.NumQuestionario, DT_CADASTRO = DateTime.Now, RESPOSTA = obj.DescResposta.ToString() });
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

    }
}
