﻿using Hypofarma.Data.Data.Interfaces.Questionario;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models.Questionario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.Json;

namespace Hypofarma.Data.Data.Questionario
{
    public sealed class QuestionarioDataService : BaseDataService, IQuestionarioDataService
    {
        readonly IRespostaQuestionarioDataService _respostaQuestionario;

        public QuestionarioDataService(ISqlDataAccess dataAccess, IRespostaQuestionarioDataService respostaQuestionario)
            : base(dataAccess) => _respostaQuestionario = respostaQuestionario;

        List<QuestionarioModel> GetAll()
        {
            try
            {
                return _dataAccess.LoadDataSync<QuestionarioModel, dynamic>("spuQuestionario_ListaTodos", new { }).ToList<QuestionarioModel>();
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }


        public string GravaFarmacovigilancia(FarmacovigilanciaModel obj)
        {
            try
            {
                _respostaQuestionario.Add(new RespostaQuestionarioModel() { NumQuestionario = this.GetAll().Where(o => o.TituloQuestionario == "Farmacovigilância").FirstOrDefault().Id, DescResposta = JsonSerializer.Serialize(obj) });
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public List<FarmacovigilanciaModel> GetFarmacovigilancia(DateTime dthCadastro)
        {
            return (from r in _respostaQuestionario.GetByDataCadastro(dthCadastro)
                    join q in this.GetAll() on r.NumQuestionario equals q.Id
                    where q.TituloQuestionario == "Farmacovigilância"
                    select JsonSerializer.Deserialize<FarmacovigilanciaModel>(r.DescResposta)).ToList();
        }

    }
}
