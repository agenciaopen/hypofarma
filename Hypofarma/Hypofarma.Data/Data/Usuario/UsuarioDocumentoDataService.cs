﻿using Hypofarma.Data.Data.Interfaces.Usuario;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models.Usuario;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data.Usuario
{
    internal sealed class UsuarioDocumentoDataService : BaseDataService, IUsuarioDocumentoDataService
    {
        public UsuarioDocumentoDataService(ISqlDataAccess dataAccess)
            : base(dataAccess) { }

        public UsuarioDocumentoModel BuscaDocumentoPeloCodigo(string codDocumento)
        {
            try
            {
                return _dataAccess.LoadDataSync<UsuarioDocumentoModel, dynamic>("spuUsuarioDocumento_PeloCodigo", new { COD_DOC = codDocumento }).FirstOrDefault(); ;
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        public List<UsuarioDocumentoModel> BuscaPeloUsuario(int Id)
        {
            try
            {
                return _dataAccess.LoadDataSync<UsuarioDocumentoModel, dynamic>("spuUsuarioDocumento_ListaPeloUsuarioId", new { ID_USUARIO = Id }).ToList<UsuarioDocumentoModel>();
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        public string Add(UsuarioDocumentoModel obj)
        {
            if (this.BuscaPeloUsuario(obj.NumUsuario).Any(o => o.CodDocumento.ToLower() == obj.CodDocumento.ToLower().Trim()))
            {
                return "Este Documento já está cadastrado para este Usuário";
            }

            if (this.BuscaDocumentoPeloCodigo(obj.CodDocumento) != null)
            {
                return "Este Documento já está cadastrado para outro Usuário";
            }

            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuUsuarioDocumento_Insere", new { ID_USUARIO = obj.NumUsuario, DOCUMENTO_COD = obj.CodDocumento, TIPO_DOCUMENTO = obj.TipoDocumento });

            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public string Remove(UsuarioDocumentoModel obj)
        {
            try
            {
                _dataAccess.SaveData("spuUsuarioDocumento_Exclui", new { ID_USUARIO = obj.NumUsuario, DOCUMENTO_COD = obj.CodDocumento, TIPO_DOCUMENTO = obj.TipoDocumento });

            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

    }
}
