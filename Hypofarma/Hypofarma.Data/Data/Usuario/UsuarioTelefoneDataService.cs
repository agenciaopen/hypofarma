﻿using Hypofarma.Data.Data.Interfaces.Usuario;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data.Usuario
{
    internal sealed class UsuarioTelefoneDataService : BaseDataService, IUsuarioTelefoneDataService
    {
        public UsuarioTelefoneDataService(ISqlDataAccess dataAccess)
            : base(dataAccess) { }

        public List<UsuarioTelefoneModel> BuscaPeloUsuario(int Id)
        {
            try
            {
                return _dataAccess.LoadDataSync<UsuarioTelefoneModel, dynamic>("spuUsuarioTelefone_ListaPeloUsuarioId", new { ID_USUARIO = Id }).ToList<UsuarioTelefoneModel>();
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        public string Add(UsuarioTelefoneModel obj)
        {
            if (this.BuscaPeloUsuario(obj.NumUsuario).Any( o => o.NumTelefone == obj.NumTelefone) )
            {
                return "Este Telefone ja esta cadastrado para este Usuário!";
            }

            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuUsuarioTelefone_Insere", new {
                                                                        ID_USUARIO = obj.NumUsuario,
                                                                        TELEFONE_NUM = obj.NumTelefone,
                                                                        TIPO_TELEFONE = (int)obj.TipoTelefone,
                                                                        IDCWHATSAPP = obj.IdcWhatsApp? "S": "N" 
                                                                      });

            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public string Remove(UsuarioTelefoneModel obj)
        {
            try
            {
                _dataAccess.SaveData("spuUsuarioTelefone_Exclui", new { ID_USUARIO = obj.NumUsuario, NUMTELEFONE = obj.NumTelefone });

            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }
    }
}
