﻿using Hypofarma.Data.Data.Interfaces.Usuario;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data.Usuario
{
    internal sealed class UsuarioLogDataService: BaseDataService, IUsuarioLogDataService
    {
        public UsuarioLogDataService(ISqlDataAccess dataAccess)
            : base(dataAccess) { }

        public List<UsuarioLogModel> BuscaPeloUsuario(int Id)
        {
            try
            {
                return _dataAccess.LoadDataSync<UsuarioLogModel, dynamic>("spuUsuarioLog_ListaPeloUsuarioId", new { ID_USUARIO = Id }).ToList<UsuarioLogModel>();
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        public string Add(UsuarioLogModel obj)
        {
            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuUsuarioLog_Insere", new { ID_USUARIO = obj.NumUsuario, DT_LOG = DateTime.Now, DESCLOG = obj.DescLog });

            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }
    }
}
