﻿using Hypofarma.Data.Data.Interfaces.Usuario;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Enum;
using Hypofarma.Data.Models;
using Hypofarma.Data.Models.Usuario;
using Open.CORE;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data.Usuario
{
    public sealed class UsuarioDataService : BaseDataService, IUsuarioDataService
    {
        readonly IUsuarioLogDataService _log;
        readonly IUsuarioTelefoneDataService _telefone;
        readonly IUsuarioDocumentoDataService _documento;
        readonly IUsuarioMunicipioDataService _municipio;
        readonly IUsuarioPerfilDataService _perfilAcesso;

        public UsuarioDataService(  ISqlDataAccess dataAccess, 
                                    IUsuarioLogDataService log, 
                                    IUsuarioTelefoneDataService telefone, 
                                    IUsuarioDocumentoDataService documento,
                                    IUsuarioMunicipioDataService municipio,
                                    IUsuarioPerfilDataService perfilAcesso)
            : base(dataAccess)
        {
            _log = log;
            _telefone = telefone;
            _documento = documento;
            _municipio = municipio;
            _perfilAcesso = perfilAcesso;
        }

        public List<UsuarioModel> GetAll()
        {
            return base.GetAll<UsuarioModel>("spuUsuario_ListaTodos");
        }

        public UsuarioModel GetById(int usuarioId)
        {
            try
            {
                return _dataAccess.LoadDataSync<UsuarioModel, dynamic>("spuUsuario_ListaPeloId", new { USUARIO_ID = usuarioId }).FirstOrDefault(); ;
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        public UsuarioModel GetByEMail(string eMail)
        {
            try
            {
                return _dataAccess.LoadDataSync<UsuarioModel, dynamic>("spuUsuario_ListaPeloEMail", new { EMAIL = eMail.ToLower().Trim() }).FirstOrDefault(); ;
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        public string Login(string eMail, string senha)
        {
            var _resultado = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(eMail))
                {
                    return "Informe o E-Mail!";
                }

                if (string.IsNullOrEmpty(senha))
                {
                    return "Informe a Senha!";
                }

                var _usuario = this.GetByEMail(eMail);

                if (_usuario == null)
                {
                    return "Usuário não encontrado!";
                }

                if (_usuario.IdcSituacao == Enum.SituacaoEnum.Inativo)
                {
                    return "Esse Usuário está Inativo. Procure o Administrador do Sistema";
                }

                if (CryptObj.Decripta(_usuario.DesSenha) != senha)
                {
                    return "Senha incorreta!";
                }

                _log.Add( new UsuarioLogModel() { NumUsuario = _usuario.Id, DescLog = "Login no Sistema"});

            }
            catch (Exception ex)
            {
                _resultado = "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }      

            return _resultado;
        }

        public string Add(UsuarioModel obj)
        {
            if (this.GetByEMail(obj.EMail) != null)
            {
                return "Já existe um Usuário cadastrado com esse E-Mail!";
            }

            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuUsuario_Insere", new { NOME = obj.NomUsuario, SENHA = CryptObj.Encripta(obj.DesSenha), EMAIL = obj.EMail });
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public string Update(UsuarioModel obj)
        {
            if (this.GetByEMail(obj.EMail).Id != obj.Id)
            {
                return "Já existe um Usuário cadastrado com esse E-Mail!";
            }

            try
            {
                obj.Validate();

                var _usuario = this.GetByEMail(obj.EMail);

                if (_usuario == null)
                {
                    return "Usuário não encontrado!";
                }
                else
                {
                    if (_usuario.Id != obj.Id)
                    {
                        return "Já existe um Usuário cadastrado com esse E-Mail!";
                    }

                    _usuario.NomUsuario = obj.NomUsuario;
                    _usuario.EMail = obj.EMail;
                    _usuario.IdcSituacao = obj.IdcSituacao;

                    _dataAccess.SaveData("spuUsuario_Altera", new { USUARIO_ID = _usuario.Id, NOME = _usuario.NomUsuario, SENHA = _usuario.DesSenha, EMAIL = _usuario.EMail, SITUACAO = GetSituacao(obj.IdcSituacao) });
                }
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public List<UsuarioLogModel> GetLog(string eMail)
        {
            return _log.BuscaPeloUsuario(this.GetByEMail(eMail).Id );
        }

        public List<PerfilAcessoModel> GetPerfil(string eMail)
        {
            return _perfilAcesso.BuscaPeloUsuario(this.GetByEMail(eMail).Id);
        }


        #region Telefone

        public List<UsuarioTelefoneModel> GetTelefone(string eMail)
        {
            return _telefone.BuscaPeloUsuario(this.GetByEMail(eMail).Id);
        }

        public string AddTelefone(UsuarioTelefoneModel obj)
        {
            return _telefone.Add(obj);
        }

        public string RemoveTelefone(UsuarioTelefoneModel obj)
        {
            return _telefone.Remove(obj);
        }

        #endregion

        #region Documento

        public List<UsuarioDocumentoModel> GetDocumento(string eMail)
        {
            return _documento.BuscaPeloUsuario(this.GetByEMail(eMail).Id);
        }

        public string AddDocumento(UsuarioDocumentoModel obj)
        {
            return _documento.Add(obj);
        }

        public string RemoveDocumento(UsuarioDocumentoModel obj)
        {
            return _documento.Remove(obj);
        }

        #endregion

        #region Municipio

        public List<MunicipioModel> GetMunicipio(string eMail)
        {
            return _municipio.BuscaPeloUsuario(this.GetByEMail(eMail).Id);
        }

        public string AddMunicipio(UsuarioMunicipioModel obj)
        {
            return _municipio.Add(obj);
        }

        public string RemoveMunicipio(UsuarioMunicipioModel obj)
        {
            return _municipio.Remove(obj);
        }

        #endregion
    }
}
