﻿using Hypofarma.Data.Data.Interfaces;
using Hypofarma.Data.Data.Interfaces.Usuario;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace Hypofarma.Data.Data.Usuario
{
    public sealed class UsuarioPerfilDataService : BaseDataService, IUsuarioPerfilDataService
    {
        readonly IPerfilAcessoDataService _perfil;

        public UsuarioPerfilDataService(ISqlDataAccess dataAccess, IPerfilAcessoDataService perfil)
            : base(dataAccess) => _perfil = perfil;

        public List<PerfilAcessoModel> BuscaPeloUsuario(int Id)
        {
            return (from l in _dataAccess.LoadDataSync<UsuarioPerfilModel, dynamic>("spuUsuarioPerfil_ListaPeloUsuarioId", new { ID_USUARIO = Id }).ToList<UsuarioPerfilModel>()
                    select _perfil.GetById(l.NumPerfil)).ToList();
        }
    }
}
