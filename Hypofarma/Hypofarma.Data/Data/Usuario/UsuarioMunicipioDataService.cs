﻿using Hypofarma.Data.Data.Interfaces;
using Hypofarma.Data.Data.Interfaces.Usuario;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data.Usuario
{
    internal sealed class UsuarioMunicipioDataService : BaseDataService, IUsuarioMunicipioDataService
    {
        readonly IMunicipioDataService _municipio;

        public UsuarioMunicipioDataService( ISqlDataAccess dataAccess, IMunicipioDataService municipio)
            : base(dataAccess) => _municipio = municipio;

        public List<MunicipioModel> BuscaPeloUsuario(int Id)
        {
            return (from l in _dataAccess.LoadDataSync<UsuarioMunicipioModel, dynamic>("spuUsuarioMunicipio_ListaPorUsuario", new { ID_USUARIO = Id }).ToList<UsuarioMunicipioModel>()
                    select _municipio.GetById(l.NumMunicipio)).ToList();
        }

        public string Add(UsuarioMunicipioModel obj)
        {
            if (_municipio.GetById(obj.NumMunicipio) == null)
            {
                return "Município não cadastrado!";
            }

            if (this.BuscaPeloUsuario(obj.NumUsuario).Count() > 0)
            {
                return "Já existe um Município associado a esse Usuário!";
            }

            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuUsuarioMunicipio_Insere", new { ID_USUARIO = obj.NumUsuario, ID_MUNICIPIO = obj.NumMunicipio });
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public string Remove(UsuarioMunicipioModel obj)
        {
            try
            {
                _dataAccess.SaveData("spuUsuarioMunicipio_Exclui", new { ID_USUARIO = obj.NumUsuario, ID_MUNICIPIO = obj.NumMunicipio });
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }
    }
}
