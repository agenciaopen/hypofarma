﻿using Hypofarma.Data.Data.Interfaces.Formulario;
using Hypofarma.Data.Data.Interfaces.Rodape;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models.Formulario;
using Hypofarma.Data.Models.Rodape;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data
{
    public sealed class RodapeDataService : BaseDataService, IRodapeDataService
    {
        IFormuarioDataService _formulario;

        public RodapeDataService(ISqlDataAccess dataAccess, IFormuarioDataService formulario)
            : base(dataAccess) => _formulario = formulario;

        public List<MidiaWebModel> GetMidaWeb()
        {
            return base.GetAll<MidiaWebModel>("spuMidiaWeb_ListaTodos");
        }

        public string AddMidaWeb(MidiaWebModel mw)
        {
            if (this.GetMidaWeb().Any(x => x.CanalWeb == mw.CanalWeb))
            {
                return "Mídia Web já cadastrada!";
            }

            try
            {
                mw.Validate();

                _dataAccess.SaveData("spuMidiaWeb_Insere", new { ID_CANAL = (int)mw.CanalWeb, LNK_PERFIL = mw.LinkPerfil });
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public string RemoveMidaWeb(MidiaWebModel mw)
        {
            try
            {
                _dataAccess.SaveData("spuMidiaWeb_Exclui", new { ID_CANAL = (int)mw.CanalWeb });
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }


        public string GravaEnderecoRodape(EnderecoRodapeModel rodape)
        {
            return _formulario.GravaEnderecoRodape(rodape);
        }

        public EnderecoRodapeModel GetRodape()
        {
            return _formulario.GetRodape();
        }

    }
}
