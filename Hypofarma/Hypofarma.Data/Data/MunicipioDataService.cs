﻿using Hypofarma.Data.Data.Interfaces;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data
{
    public sealed class MunicipioDataService : BaseDataService, IMunicipioDataService
    {
        readonly IParametroSistemaService _parametro;
        readonly IEstadoDataService _estado;

        public MunicipioDataService(ISqlDataAccess dataAccess, 
                                    IParametroSistemaService parametro, 
                                    IEstadoDataService estado)
            : base(dataAccess)
        {
            _parametro = parametro;
            _estado = estado;
        }

        public List<MunicipioModel> GetByEstado(string sigla)
        {
            try
            {
                return _dataAccess.LoadDataSync<MunicipioModel, dynamic>("spuMunicipio_ListaPorEstado", new { ID_ESTADO = _estado.GetBySigla(sigla).Id }).ToList<MunicipioModel>();
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        public void CargaTodasCidades()
        {
            foreach (var est in _estado.GetAll())
            {
               var _lstCidades = (  from l in new RestClient().Execute<List<MunicipioModel>>(new RestRequest(string.Format(_parametro.GetByCodigo("UrlApiIBGEMunicipioPorEstado").ValParametro, est.Sigla), Method.GET)).Data
                                    select new MunicipioModel() { Id = l.Id, Nome = l.Nome, NumEstado = est.Id }).ToList();

                foreach (var item in _lstCidades.Where(o => !this.GetByEstado(est.Sigla).Any(x => x.Id == o.Id)))
                {
                    var _msg = this.Add(item);

                    if (!string.IsNullOrEmpty(_msg))
                    {
                        throw new Exception(_msg);
                    }
                }
            }
        }

        List<MunicipioModel> GetList()
        {
            try
            {
                return _dataAccess.LoadDataSync<MunicipioModel, dynamic>("spuMunicipio_ListaTodos", new {  }).ToList<MunicipioModel>();
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        string Add(MunicipioModel obj)
        {
            try
            {
                if (this.GetById(obj.Id) != null)
                {
                    return "Já existe um Município cadastrado com esse Código!";
                }

                _dataAccess.SaveData("spuMunicipio_Insere", new { ID_MUNICIPIO = obj.Id, DESCRICAO = obj.Nome, ID_ESTADO = obj.NumEstado });
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public MunicipioModel GetById(int municipioId)
        {
            var _lst = this.GetList().Where(o => o.Id == municipioId);

            return _lst.Count() == 0 ? null : _lst.FirstOrDefault();
        }
    }
}
