﻿using Hypofarma.Data.Data.Interfaces;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data
{
    public sealed class PerfilAcessoDataService : BaseDataService, IPerfilAcessoDataService
    {
        public PerfilAcessoDataService(ISqlDataAccess dataAccess)
            : base(dataAccess) { }

        List<PerfilAcessoModel> GetAll()
        {
            try
            {
                return _dataAccess.LoadDataSync<PerfilAcessoModel, dynamic>("spuPerfilAcesso_ListaTodos", new { }).ToList<PerfilAcessoModel>();
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        public PerfilAcessoModel GetById(int id)
        {
            return this.GetAll().Where(o => o.Id == id).FirstOrDefault();
        }
    }
}
