﻿using Hypofarma.Data.Data.Interfaces.Produto;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data.Produto
{
    public sealed class EmbalagemDataService: BaseDataService, IEmbalagemDataService
    {
        public EmbalagemDataService(ISqlDataAccess dataAccess)
            : base(dataAccess) { }

        public List<EmbalagemModel> GetAll()
        {
            return base.GetAll<EmbalagemModel>("spuEmbalagem_ListaTodos");
        }

        public EmbalagemModel GetById(int id)
        {
            return this.GetAll().Where(o => o.Id == id).FirstOrDefault();
        }

        public string Add(EmbalagemModel obj)
        {
            if (this.GetAll().Any(x => x.DescEmbalagem.ToLower() == obj.DescEmbalagem.ToLower().Trim()))
            {
                return "Embalagem já cadastrada!";
            }

            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuEmbalagem_Insere", new { DESCRICAO = obj.DescEmbalagem, ARQ_ICONE = obj.ArqIconeEmbalagem, ID_USUARIO = obj.NumUsuarioCadastro });
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public string Update(EmbalagemModel obj)
        {
            if (this.GetAll().Any(x => x.DescEmbalagem.ToLower() == obj.DescEmbalagem.ToLower().Trim() && x.Id != obj.Id))
            {
                return "Embalagem já cadastrada!";
            }

            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuEmbalagem_Altera", new { ID_EMBALAGEM = obj.Id, DESCRICAO = obj.DescEmbalagem, ARQ_ICONE = obj.ArqIconeEmbalagem, ATIVO = GetSituacao(obj.IdcSituacao) });
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public string Remove(EmbalagemModel obj)
        {
            return base.Remove("spuEmbalagem_Exclui", obj.Id);
        }

    }
}
