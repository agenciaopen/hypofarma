﻿using Hypofarma.Data.Data.Interfaces.Produto;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data.Produto
{
    public sealed class CategoriaRegulatoriaDataService : BaseDataService, ICategoriaRegulatoriaDataService
    {
        public CategoriaRegulatoriaDataService(ISqlDataAccess dataAccess)
            : base(dataAccess) { }

        public List<CategoriaRegulatoriaModel> GetAll()
        {
            return base.GetAll<CategoriaRegulatoriaModel>("spuCategoriaRegulatoria_ListaTodos");
        }

        public CategoriaRegulatoriaModel GetById(int id)
        {
            return this.GetAll().Where(o => o.Id == id).FirstOrDefault();
        }

        public string Add(CategoriaRegulatoriaModel obj)
        {
            if (this.GetAll().Any(x => x.DescCategoriaRegulatoria.ToLower() == obj.DescCategoriaRegulatoria.ToLower().Trim()))
            {
                return "Categoria Regulatória já cadastrada!";
            }

            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuCategoriaRegulatoria_Insere", new { DESCRICAO = obj.DescCategoriaRegulatoria, ID_USUARIO = obj.NumUsuarioCadastro });
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public string Update(CategoriaRegulatoriaModel obj)
        {
            if (this.GetAll().Any(x => x.DescCategoriaRegulatoria.ToLower() == obj.DescCategoriaRegulatoria.ToLower().Trim() && x.Id != obj.Id))
            {
                return "Categoria Regulatória já cadastrada!";
            }

            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuCategoriaRegulatoria_Altera", new { ID_CATEGORIA = obj.Id, DESCRICAO = obj.DescCategoriaRegulatoria, ATIVO = GetSituacao(obj.IdcSituacao) });
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public string Remove(CategoriaRegulatoriaModel obj)
        {
            return base.Remove("spuCategoriaRegulatoria_Exclui", obj.Id);
        }

    }
}
