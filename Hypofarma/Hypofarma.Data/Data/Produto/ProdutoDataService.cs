﻿using Hypofarma.Data.Data.Interfaces.Produto;
using Hypofarma.Data.Data.Interfaces.Usuario;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data.Produto
{
    public sealed class ProdutoDataService: BaseDataService, IProdutoDataService
    {
        readonly ICategoriaRegulatoriaDataService _categoriaRegulatoria;
        readonly IClasseTerapeuticaDataService _classeTerapeutica;
        readonly IEmbalagemDataService _embalagem;
        readonly IPrincipioAtivoDataService _principioAtivo;
        readonly IUsuarioDataService _usuario;
        readonly IProdutoDocumentoDataService _documento;

        public ProdutoDataService(  ISqlDataAccess dataAccess,
                                    ICategoriaRegulatoriaDataService categoriaRegulatoria,
                                    IClasseTerapeuticaDataService classeTerapeutica,
                                    IEmbalagemDataService embalagem,
                                    IPrincipioAtivoDataService principioAtivo,
                                    IUsuarioDataService usuario,
                                    IProdutoDocumentoDataService documento)
            : base(dataAccess) 
        {
            _categoriaRegulatoria = categoriaRegulatoria;
            _classeTerapeutica = classeTerapeutica;
            _embalagem = embalagem;
            _principioAtivo = principioAtivo;
            _usuario = usuario;
            _documento = documento;
        }

        ProdutoViewModel GetProduto(ProdutoModel pro)
        {
            return new ProdutoViewModel()
            {
                Id = pro.Id,
                NomProduto = pro.NomProduto,
                Concentracao = pro.Concentracao,
                Apresentacao = pro.Apresentacao,
                Registro = pro.Registro,
                ImgBackGround = pro.ImgBackGround,
                Indicacao = pro.Indicacao,
                IdcGenerico = pro.IdcGenerico,
                IdcExibeVitrine = pro.IdcExibeVitrine,
                IdcSituacao = pro.IdcSituacao,
                CategoriaRegulatoria = _categoriaRegulatoria.GetById(pro.NumCategoriaRegulatoria),
                ClasseTerapeutica = _classeTerapeutica.GetById(pro.NumClasseTerapeutica),
                Embalagem = _embalagem.GetById(pro.NumEmbalagem),
                PrincipioAtivo = _principioAtivo.GetById(pro.NumPrincipioAtivo),
                UsuarioCadastro = _usuario.GetById(pro.NumUsuarioCadastro)
            };
        }

        public List<ProdutoViewModel> GetByCategoriaRegulatoria(int idCategoriaRegulatoria)
        {
            try
            {
                return (from l in _dataAccess.LoadDataSync<ProdutoModel, dynamic>("spuProduto_ListaFiltro", new {   ID_PRODUTO = 0, ID_CATEGORIA = idCategoriaRegulatoria, ID_CLASSE = 0, ID_EMBALAGEM = 0, ID_PRINCIPIO_ATIVO = 0 }).ToList<ProdutoModel>()
                        select GetProduto(l)).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        public List<ProdutoViewModel> GetByClasseTerapeutica(int idClasseTerapeutica)
        {
            try
            {
                return (from l in _dataAccess.LoadDataSync<ProdutoModel, dynamic>("spuProduto_ListaFiltro", new { ID_PRODUTO = 0, ID_CATEGORIA = 0, ID_CLASSE = idClasseTerapeutica, ID_EMBALAGEM = 0, ID_PRINCIPIO_ATIVO = 0 }).ToList<ProdutoModel>()
                        select GetProduto(l)).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        public List<ProdutoViewModel> GetByEmbalagem(int idEmbalagem)
        {
            try
            {
                return (from l in _dataAccess.LoadDataSync<ProdutoModel, dynamic>("spuProduto_ListaFiltro", new { ID_PRODUTO = 0, ID_CATEGORIA = 0, ID_CLASSE = 0, ID_EMBALAGEM = idEmbalagem, ID_PRINCIPIO_ATIVO = 0 }).ToList<ProdutoModel>()
                        select GetProduto(l)).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        public List<ProdutoViewModel> GetByPrincipioAtivo(int idPrincipioAtivo)
        {
            try
            {
                return (from l in _dataAccess.LoadDataSync<ProdutoModel, dynamic>("spuProduto_ListaFiltro", new { ID_PRODUTO = 0, ID_CATEGORIA = 0, ID_CLASSE = 0, ID_EMBALAGEM = 0, ID_PRINCIPIO_ATIVO = idPrincipioAtivo }).ToList<ProdutoModel>()
                        select GetProduto(l)).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        public List<ProdutoViewModel> GetAll()
        {
            try
            {
                return (from l in _dataAccess.LoadDataSync<ProdutoModel, dynamic>("spuProduto_ListaFiltro", new { ID_PRODUTO = 0, ID_CATEGORIA = 0, ID_CLASSE = 0, ID_EMBALAGEM = 0, ID_PRINCIPIO_ATIVO = 0 }).ToList<ProdutoModel>()
                        select GetProduto(l)).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        public ProdutoViewModel GetById(int id)
        {
            try
            {
                return (from l in _dataAccess.LoadDataSync<ProdutoModel, dynamic>("spuProduto_ListaFiltro", new { ID_PRODUTO = id, ID_CATEGORIA = 0, ID_CLASSE = 0, ID_EMBALAGEM = 0, ID_PRINCIPIO_ATIVO = 0 }).ToList<ProdutoModel>()
                        select GetProduto(l)).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }


        public string Add(ProdutoModel obj)
        {
            if (this.GetAll().Any(x => x.NomProduto.ToLower() == obj.NomProduto.ToLower().Trim()))
            {
                return "Já existe um Produto cadastrado com esse Nome!";
            }

            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuProduto_Insere", new { 
                                                                NOME = obj.NomProduto,
                                                                CONCENTRACAO = obj.Concentracao,
                                                                APRESENTACAO = obj.Apresentacao,
                                                                REGISTRO = obj.Registro,
                                                                IMG_BACK = obj.ImgBackGround,
                                                                INDICACAO = obj.Indicacao,
                                                                EH_GENERICO = obj.IdcGenerico? "S": "N",
                                                                EXIBE_VITRINE = obj.IdcExibeVitrine? "S": "N",
                                                                ID_CATEGORIA = obj.NumCategoriaRegulatoria,
                                                                ID_CLASSE = obj.NumClasseTerapeutica,
                                                                ID_EMBALAGEM = obj.NumEmbalagem,
                                                                ID_PRINCIPIO_ATIVO = obj.NumPrincipioAtivo,
                                                                ID_USUARIO_CADASTRO = obj.NumUsuarioCadastro,
                                                                ID_USUARIO = obj.NumUsuarioCadastro 
                                                              });
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public string Update(ProdutoModel obj)
        {
            if (this.GetAll().Any(x => x.NomProduto.ToLower() == obj.NomProduto.ToLower().Trim() && x.Id != obj.Id))
            {
                return "Já existe um Produto cadastrado com esse Nome!";
            }

            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuProduto_Altera", new { ID_PRODUTO = obj.Id,
                                                                NOME = obj.NomProduto,
                                                                CONCENTRACAO = obj.Concentracao,
                                                                APRESENTACAO = obj.Apresentacao,
                                                                REGISTRO = obj.Registro,
                                                                IMG_BACK = obj.ImgBackGround,
                                                                INDICACAO = obj.Indicacao,
                                                                ATIVO = GetSituacao(obj.IdcSituacao),
                                                                EH_GENERICO = obj.IdcGenerico ? "S" : "N",
                                                                EXIBE_VITRINE = obj.IdcExibeVitrine ? "S" : "N",                    
                                                                ID_CATEGORIA = obj.NumCategoriaRegulatoria,
                                                                ID_CLASSE = obj.NumClasseTerapeutica,
                                                                ID_EMBALAGEM = obj.NumEmbalagem,
                                                                ID_PRINCIPIO_ATIVO = obj.NumPrincipioAtivo,
                                                                ID_USUARIO_CADASTRO = obj.NumUsuarioCadastro
                                                            });
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }


        public List<ProdutoDocumentoModel> GetDocumento(int idProduto)
        {
            return _documento.BuscaPeloProduto(idProduto);
        }

        public string AddDocumento(ProdutoDocumentoModel doc)
        {
            return _documento.Add(doc);
        }

    }
}
