﻿using Hypofarma.Data.Data.Interfaces.Produto;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data.Produto
{
    internal sealed class ProdutoDocumentoDataService : BaseDataService, IProdutoDocumentoDataService
    {
        public ProdutoDocumentoDataService(ISqlDataAccess dataAccess)
            : base(dataAccess) { }

        public List<ProdutoDocumentoModel> BuscaPeloProduto(int id)
        {
            try
            {
                return _dataAccess.LoadDataSync<ProdutoDocumentoModel, dynamic>("spuProdutoDocumento_ListaPorProduto", new { ID_PRODUTO = id }).ToList<ProdutoDocumentoModel>();
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        public string Add(ProdutoDocumentoModel obj)
        {
            if (this.BuscaPeloProduto(obj.NumProduto).Any(x => x.IdcEscopo ==  obj.IdcEscopo && x.NomArquivo.ToLower() == obj.NomArquivo.ToLower().Trim()))
            {
                return "Arquivo já cadastrado!";
            }

            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuProdutoDocumento_Insere", new { ID_PRODUTO = obj.NumProduto, NOME_ARQUIVO = obj.NomArquivo, ARQ_PRODUTO = obj.ArqProduto, ESCOPO = obj.IdcEscopo == Enum.EscopoArquivoProdutoEnum.Medico? "M": "P" });
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public string Remove(ProdutoDocumentoModel obj)
        {
            try
            {
                _dataAccess.SaveData("spuProdutoDocumento_Exclui", new { ID_DOCUMENTO = obj.Id, ID_PRODUTO = obj.NumProduto });
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }
    }
}
