﻿using Hypofarma.Data.Data.Interfaces.Produto;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data.Produto
{
    public sealed class PrincipioAtivoDataService : BaseDataService, IPrincipioAtivoDataService
    {
        public PrincipioAtivoDataService(ISqlDataAccess dataAccess)
            : base(dataAccess) { }

        public List<PrincipioAtivoModel> GetAll()
        {
            return base.GetAll<PrincipioAtivoModel>("spuPrincipioAtivo_ListaTodos");
        }

        public PrincipioAtivoModel GetById(int id)
        {
            return this.GetAll().Where(o => o.Id == id).FirstOrDefault();
        }

        public string Add(PrincipioAtivoModel obj)
        {
            if (this.GetAll().Any(x => x.DescPrincipioAtivo.ToLower() == obj.DescPrincipioAtivo.ToLower().Trim()))
            {
                return "Princípio Ativo já cadastrado!";
            }

            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuPrincipioAtivo_Insere", new { DESCRICAO = obj.DescPrincipioAtivo, ID_USUARIO = obj.NumUsuarioCadastro });
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public string Update(PrincipioAtivoModel obj)
        {
            if (this.GetAll().Any(x => x.DescPrincipioAtivo.ToLower() == obj.DescPrincipioAtivo.ToLower().Trim() && x.Id != obj.Id))
            {
                return "Princípio Ativo já cadastrado!";
            }

            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuPrincipioAtivo_Altera", new { ID_PRINCIPIO = obj.Id, DESCRICAO = obj.DescPrincipioAtivo, ATIVO = GetSituacao(obj.IdcSituacao) });
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public string Remove(PrincipioAtivoModel obj)
        {
            return base.Remove("spuPrincipioAtivo_Exclui", obj.Id);
        }
    }
}
