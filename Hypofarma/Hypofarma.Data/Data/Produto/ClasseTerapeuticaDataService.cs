﻿using Hypofarma.Data.Data.Interfaces.Produto;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data.Produto
{
    public sealed class ClasseTerapeuticaDataService : BaseDataService, IClasseTerapeuticaDataService
    {
        public ClasseTerapeuticaDataService(ISqlDataAccess dataAccess)
            : base(dataAccess) { }

        public List<ClasseTerapeuticaModel> GetAll()
        {
            return base.GetAll<ClasseTerapeuticaModel>("spuClasseTerapeutica_ListaTodos");
        }

        public ClasseTerapeuticaModel GetById(int id)
        {
            return this.GetAll().Where(o => o.Id == id).FirstOrDefault();
        }

        public string Add(ClasseTerapeuticaModel obj)
        {
            if (this.GetAll().Any(x => x.DescClasseTerapeutica.ToLower() == obj.DescClasseTerapeutica.ToLower().Trim()))
            {
                return "Classe Terapeutica já cadastrada";
            }

            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuClasseTerapeutica_Insere", new { DESCRICAO = obj.DescClasseTerapeutica, ID_USUARIO = obj.NumUsuarioCadastro });
            }
            catch (ValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public string Update(ClasseTerapeuticaModel obj)
        {
            if (this.GetAll().Any(x => x.DescClasseTerapeutica.ToLower() == obj.DescClasseTerapeutica.ToLower().Trim() && x.Id != obj.Id))
            {
                return "Classe Terapeutica já cadastrada!";
            }

            try
            {
                obj.Validate();

                _dataAccess.SaveData("spuClasseTerapeutica_Altera", new { ID_CLASSE = obj.Id, CLASSE = obj.DescClasseTerapeutica, ATIVO = GetSituacao(obj.IdcSituacao) });
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public string Remove(ClasseTerapeuticaModel obj)
        {
            return base.Remove("spuClasseTerapeutica_Exclui", obj.Id);
        }
    }
}
