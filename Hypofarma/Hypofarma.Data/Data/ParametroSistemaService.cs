﻿using Hypofarma.Data.Data.Interfaces;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data
{
    internal sealed class ParametroSistemaService : BaseDataService, IParametroSistemaService
    {
        public ParametroSistemaService(ISqlDataAccess dataAccess)
            : base(dataAccess) { }

        List<ParametroSistemaModel> GetAll()
        {
            try
            {
                return _dataAccess.LoadDataSync<ParametroSistemaModel, dynamic>("spuPArametroSistema_ListaTodos", new { }).ToList<ParametroSistemaModel>();
            }
            catch (Exception ex)
            {
                throw new Exception("ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message);
            }
        }

        public ParametroSistemaModel GetByCodigo(string codParametro)
        {
            return this.GetAll().Where(o => o.CodParametro == codParametro).FirstOrDefault();
        }
    }
}
