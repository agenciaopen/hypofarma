﻿using Hypofarma.Data.Data.Interfaces;
using Hypofarma.Data.DataAccess;
using Hypofarma.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Hypofarma.Data.Data
{
    public sealed class EstadoDataService : BaseDataService, IEstadoDataService
    {
        public EstadoDataService(ISqlDataAccess dataAccess)
            : base(dataAccess) { }

        public List<EstadoModel> GetAll()
        {
            return base.GetAll<EstadoModel>("spuEstado_ListaTodos");
        }

        string Add(EstadoModel obj)
        {
            try
            {
                if (this.GetBySigla(obj.Sigla) != null)
                {
                    return "Já existe um Estado cadastrado com essa Sigla!";
                }

                _dataAccess.SaveData("spuEstado_Insere", new { ID_ESTADO = obj.Id, SIGLA = obj.Sigla, DESCRICAO = obj.Nome });
            }
            catch (Exception ex)
            {
                return "ERRO " + this.GetType().Name + "." + MethodBase.GetCurrentMethod() + "(): " + ex.Message;
            }

            return string.Empty;
        }

        public EstadoModel GetById(int estadoId)
        {
            var _lst = this.GetAll().Where(o => o.Id == estadoId);

            return _lst.Count() == 0? null: _lst.FirstOrDefault();
        }

        public EstadoModel GetBySigla(string sigla)
        {
            var _lst = this.GetAll().Where(o => o.Sigla == sigla);

            return _lst.Count() == 0? null: _lst.FirstOrDefault();
        }
        
    }
}
