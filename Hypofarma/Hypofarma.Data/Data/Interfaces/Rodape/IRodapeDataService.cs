﻿using Hypofarma.Data.Models.Formulario;
using Hypofarma.Data.Models.Rodape;
using System.Collections.Generic;

namespace Hypofarma.Data.Data.Interfaces.Rodape
{
    public interface IRodapeDataService
    {
        public List<MidiaWebModel> GetMidaWeb();

        public string AddMidaWeb(MidiaWebModel mw);

        public string RemoveMidaWeb(MidiaWebModel mw);

        string GravaEnderecoRodape(EnderecoRodapeModel rodape);

        EnderecoRodapeModel GetRodape();
    }
}
