﻿using Hypofarma.Data.Models;
using System.Collections.Generic;

namespace Hypofarma.Data.Data.Interfaces
{
    public interface IParametroSistemaService
    {
        ParametroSistemaModel GetByCodigo(string codParametro);
    }
}
