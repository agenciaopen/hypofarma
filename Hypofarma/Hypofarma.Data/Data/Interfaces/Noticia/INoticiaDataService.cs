﻿using Hypofarma.Data.Data.Interfaces.CRUD;
using Hypofarma.Data.Models.Noticia;
using System.Collections.Generic;

namespace Hypofarma.Data.Data.Interfaces.Noticia
{
    public interface INoticiaDataService:   IGetAllService<NoticiaModel>, 
                                            IAddService<NoticiaModel>, 
                                            IUpdateService<NoticiaModel>, 
                                            IRemoveService<NoticiaModel>
    {
        List<NoticiaImgModel> GetImg(int idNoticia);

        string AddImg(NoticiaImgModel img);

        string RemoveImg(NoticiaImgModel img);
    }
}
