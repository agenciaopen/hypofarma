﻿using Hypofarma.Data.Data.Interfaces.CRUD;
using Hypofarma.Data.Models.Noticia;
using System.Collections.Generic;

namespace Hypofarma.Data.Data.Interfaces.Noticia
{
    public interface INoticiaImgDataService: IAddService<NoticiaImgModel>, IRemoveService<NoticiaImgModel>
    {
        List<NoticiaImgModel> BuscaPelaNoticia(int Id);
    }
}
