﻿using Hypofarma.Data.Data.Interfaces.CRUD;
using Hypofarma.Data.Models;
using System.Collections.Generic;

namespace Hypofarma.Data.Data.Interfaces
{
    public interface IEstadoDataService: IGetByIdService<EstadoModel>, IGetAllService<EstadoModel>
    { 
        public EstadoModel GetBySigla(string sigla);        
    }
}
