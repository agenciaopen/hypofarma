﻿using Hypofarma.Data.Data.Interfaces.CRUD;
using Hypofarma.Data.Models;
using System.Collections.Generic;

namespace Hypofarma.Data.Data.Interfaces
{
    public interface IMunicipioDataService: IGetByIdService<MunicipioModel>
    {
        List<MunicipioModel> GetByEstado(string sigla);

        void CargaTodasCidades();
    }
}
