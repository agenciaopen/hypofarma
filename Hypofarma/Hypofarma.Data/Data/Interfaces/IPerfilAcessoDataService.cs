﻿using Hypofarma.Data.Data.Interfaces.CRUD;
using Hypofarma.Data.Models;

namespace Hypofarma.Data.Data.Interfaces
{
    public interface IPerfilAcessoDataService : IGetByIdService<PerfilAcessoModel>
    {
    }
}
