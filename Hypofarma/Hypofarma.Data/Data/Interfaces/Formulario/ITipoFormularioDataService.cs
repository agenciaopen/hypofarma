﻿using Hypofarma.Data.Data.Interfaces.CRUD;
using Hypofarma.Data.Models.Formulario;

namespace Hypofarma.Data.Data.Interfaces.Formulario
{
    public interface ITipoFormularioDataService: IGetAllService<TipoFormularioModel>, IGetByIdService<TipoFormularioModel>
    {

    }
}
