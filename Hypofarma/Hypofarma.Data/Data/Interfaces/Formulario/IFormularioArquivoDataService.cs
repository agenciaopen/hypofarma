﻿using Hypofarma.Data.Models.Formulario;
using System.Collections.Generic;

namespace Hypofarma.Data.Data.Interfaces.Formulario
{
    public interface IFormularioArquivoDataService
    {
        List<FormularioArquivoModel> GetPeloTipo(int idTipo);

        string Add(FormularioArquivoModel obj);

        string Remove(FormularioArquivoModel obj);
    }
}
