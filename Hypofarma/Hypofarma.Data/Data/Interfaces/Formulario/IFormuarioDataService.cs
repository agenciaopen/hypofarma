﻿using Hypofarma.Data.Models.Formulario;
using System.Collections.Generic;

namespace Hypofarma.Data.Data.Interfaces.Formulario
{
    public interface IFormuarioDataService
    {
        string GravaBannerPrincipal(BannerPrincipalModel banner, List<ArquivoDataView> arquivos);

        BannerPrincipalModel GetBannerPrincipal();


        string GravaApresentacaoHypofarma(ConhecerAHypofarmaModel apresentacao, List<ArquivoDataView> arquivos);

        ConhecerAHypofarmaModel GetApresentacao();


        string GravaEnderecoRodape(EnderecoRodapeModel rodape);

        EnderecoRodapeModel GetRodape();


        string GravaQuemSomos(QuemSomosModel frm);

        QuemSomosModel GetSomos();


        string GravaVideoSection(VideoSectionModel vs);

        VideoSectionModel GetVideoSection();


        string GravaAreaAtuacao(AreaAtuacaoModel area, List<ArquivoDataView> arquivos);

        AreaAtuacaoModel GetAreaAtuacao();

    }
}
