﻿
namespace Hypofarma.Data.Data.Interfaces.CRUD
{
    public interface IGetByIdService<T> where T : class
    {
        T GetById(int id);
    }
}
