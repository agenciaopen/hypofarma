﻿
namespace Hypofarma.Data.Data.Interfaces.CRUD
{
    public interface IUpdateService<T> where T : class
    {
        string Update(T obj);
    }
}
