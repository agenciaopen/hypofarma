﻿using System.Collections.Generic;

namespace Hypofarma.Data.Data.Interfaces.CRUD
{
    public interface IGetAllService<T> where T : class
    {
        List<T> GetAll();
    }
}
