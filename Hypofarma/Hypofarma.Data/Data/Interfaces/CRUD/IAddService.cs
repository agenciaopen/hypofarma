﻿
namespace Hypofarma.Data.Data.Interfaces.CRUD
{
    public interface IAddService<T> where T : class
    {
        string Add(T obj);
    }
}
