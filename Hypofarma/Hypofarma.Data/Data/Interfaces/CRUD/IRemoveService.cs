﻿
namespace Hypofarma.Data.Data.Interfaces.CRUD
{
    public interface IRemoveService<T> where T : class
    {
        string Remove(T obj);
    }
}
