﻿using Hypofarma.Data.Data.Interfaces.CRUD;
using Hypofarma.Data.Models;
using System.Collections.Generic;

namespace Hypofarma.Data.Data.Interfaces.Usuario
{
    public interface IUsuarioLogDataService : IAddService<UsuarioLogModel>
    {
        List<UsuarioLogModel> BuscaPeloUsuario(int Id);
    }
}
