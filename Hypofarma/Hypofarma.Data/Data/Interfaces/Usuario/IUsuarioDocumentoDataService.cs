﻿using Hypofarma.Data.Data.Interfaces.CRUD;
using Hypofarma.Data.Models.Usuario;
using System.Collections.Generic;

namespace Hypofarma.Data.Data.Interfaces.Usuario
{
    public interface IUsuarioDocumentoDataService : IAddService<UsuarioDocumentoModel>, IRemoveService<UsuarioDocumentoModel>
    {
        List<UsuarioDocumentoModel> BuscaPeloUsuario(int Id);

        UsuarioDocumentoModel BuscaDocumentoPeloCodigo(string codDocumento);
    }
}
