﻿using Hypofarma.Data.Data.Interfaces.CRUD;
using Hypofarma.Data.Models;
using System.Collections.Generic;

namespace Hypofarma.Data.Data.Interfaces.Usuario
{
    public interface IUsuarioMunicipioDataService : IAddService<UsuarioMunicipioModel>, IRemoveService<UsuarioMunicipioModel>
    {
        List<MunicipioModel> BuscaPeloUsuario(int Id);
    }
}
