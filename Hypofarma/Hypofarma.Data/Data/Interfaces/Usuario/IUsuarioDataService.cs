﻿using Hypofarma.Data.Data.Interfaces.CRUD;
using Hypofarma.Data.Models;
using Hypofarma.Data.Models.Usuario;
using System.Collections.Generic;

namespace Hypofarma.Data.Data.Interfaces.Usuario
{
    public interface IUsuarioDataService:   IGetAllService<UsuarioModel>, 
                                            IGetByIdService<UsuarioModel>, 
                                            IAddService<UsuarioModel>, 
                                            IUpdateService<UsuarioModel>
    {
        UsuarioModel GetByEMail(string eMail);

        string Login(string eMail, string senha);            

        List<UsuarioLogModel> GetLog(string eMail);

        List<PerfilAcessoModel> GetPerfil(string eMail);

        List<UsuarioTelefoneModel> GetTelefone(string eMail);
        string AddTelefone(UsuarioTelefoneModel obj);
        string RemoveTelefone(UsuarioTelefoneModel obj);

        List<UsuarioDocumentoModel> GetDocumento(string eMail);
        string AddDocumento(UsuarioDocumentoModel obj);
        string RemoveDocumento(UsuarioDocumentoModel obj);

        List<MunicipioModel> GetMunicipio(string eMail);
        string AddMunicipio(UsuarioMunicipioModel obj);
        string RemoveMunicipio(UsuarioMunicipioModel obj);
    }
}
