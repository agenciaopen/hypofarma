﻿using Hypofarma.Data.Models;
using System.Collections.Generic;

namespace Hypofarma.Data.Data.Interfaces.Usuario
{
    public interface IUsuarioPerfilDataService
    {
        List<PerfilAcessoModel> BuscaPeloUsuario(int Id);
    }
}
