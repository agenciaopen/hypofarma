﻿using Hypofarma.Data.Data.Interfaces.CRUD;
using Hypofarma.Data.Models;
using System.Collections.Generic;

namespace Hypofarma.Data.Data.Interfaces.Usuario
{
    public interface IUsuarioTelefoneDataService: IAddService<UsuarioTelefoneModel>, IRemoveService<UsuarioTelefoneModel>
    {
        List<UsuarioTelefoneModel> BuscaPeloUsuario(int Id);
    }
}
