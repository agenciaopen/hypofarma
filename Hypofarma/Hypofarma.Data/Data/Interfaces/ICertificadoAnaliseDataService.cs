﻿using Hypofarma.Data.Data.Interfaces.CRUD;
using Hypofarma.Data.Models;
using System.Collections.Generic;

namespace Hypofarma.Data.Data.Interfaces
{
    public interface ICertificadoAnaliseDataService :   IGetAllService<CertificadoAnaliseModel>, 
                                                        IGetByIdService<CertificadoAnaliseModel>, 
                                                        IAddService<CertificadoAnaliseModel>,
                                                        IRemoveService<CertificadoAnaliseModel>
    {
        List<CertificadoAnaliseModel> GetByNomeArquivo(string nomArquivo);

        string RegistraLogAcessoArquivo(CertificadoAnaliseModel arquivo, UsuarioModel usuario);
    }
}
