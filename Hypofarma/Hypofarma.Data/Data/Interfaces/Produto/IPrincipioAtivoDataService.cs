﻿using Hypofarma.Data.Data.Interfaces.CRUD;
using Hypofarma.Data.Models.Produto;

namespace Hypofarma.Data.Data.Interfaces.Produto
{
    public interface IPrincipioAtivoDataService :   IGetAllService<PrincipioAtivoModel>, 
                                                    IGetByIdService<PrincipioAtivoModel>, 
                                                    IAddService<PrincipioAtivoModel>, 
                                                    IUpdateService<PrincipioAtivoModel>, 
                                                    IRemoveService<PrincipioAtivoModel>
    {
    }
}
