﻿using Hypofarma.Data.Data.Interfaces.CRUD;
using Hypofarma.Data.Models.Produto;

namespace Hypofarma.Data.Data.Interfaces.Produto
{
    public interface ICategoriaRegulatoriaDataService : IGetAllService<CategoriaRegulatoriaModel>, 
                                                        IGetByIdService<CategoriaRegulatoriaModel>, 
                                                        IAddService<CategoriaRegulatoriaModel>, 
                                                        IUpdateService<CategoriaRegulatoriaModel>, 
                                                        IRemoveService<CategoriaRegulatoriaModel>
    {
    }
}
