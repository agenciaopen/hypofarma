﻿using Hypofarma.Data.Data.Interfaces.CRUD;
using Hypofarma.Data.Models.Produto;
using System.Collections.Generic;

namespace Hypofarma.Data.Data.Interfaces.Produto
{
    public interface IProdutoDataService : IGetAllService<ProdutoViewModel>,
                                           IGetByIdService<ProdutoViewModel>,
                                           IAddService<ProdutoModel>,
                                           IUpdateService<ProdutoModel>
    {
        public List<ProdutoViewModel> GetByCategoriaRegulatoria(int idCategoriaRegulatoria);

        public List<ProdutoViewModel> GetByClasseTerapeutica(int idClasseTerapeutica);

        public List<ProdutoViewModel> GetByEmbalagem(int idEmbalagem);

        public List<ProdutoViewModel> GetByPrincipioAtivo(int idPrincipioAtivo);


        public List<ProdutoDocumentoModel> GetDocumento(int idProduto);

        public string AddDocumento(ProdutoDocumentoModel doc);

    }
}
