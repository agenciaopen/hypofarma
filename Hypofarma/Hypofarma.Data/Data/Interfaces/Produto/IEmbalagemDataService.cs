﻿using Hypofarma.Data.Data.Interfaces.CRUD;
using Hypofarma.Data.Models.Produto;

namespace Hypofarma.Data.Data.Interfaces.Produto
{
    public interface IEmbalagemDataService : IGetAllService<EmbalagemModel>,
                                             IGetByIdService<EmbalagemModel>,
                                             IAddService<EmbalagemModel>,
                                             IUpdateService<EmbalagemModel>,
                                             IRemoveService<EmbalagemModel>
    {
    }
}
