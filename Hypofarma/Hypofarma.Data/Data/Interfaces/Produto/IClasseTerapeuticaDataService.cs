﻿using Hypofarma.Data.Data.Interfaces.CRUD;
using Hypofarma.Data.Models.Produto;

namespace Hypofarma.Data.Data.Interfaces.Produto
{
    public interface IClasseTerapeuticaDataService :    IGetAllService<ClasseTerapeuticaModel>, 
                                                        IGetByIdService<ClasseTerapeuticaModel>, 
                                                        IAddService<ClasseTerapeuticaModel>, 
                                                        IUpdateService<ClasseTerapeuticaModel>, 
                                                        IRemoveService<ClasseTerapeuticaModel>
    {
    }
}
