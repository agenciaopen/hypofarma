﻿using Hypofarma.Data.Data.Interfaces.CRUD;
using Hypofarma.Data.Models.Produto;
using System.Collections.Generic;

namespace Hypofarma.Data.Data.Interfaces.Produto
{
    public interface IProdutoDocumentoDataService : IAddService<ProdutoDocumentoModel>, IRemoveService<ProdutoDocumentoModel>
    {
        public List<ProdutoDocumentoModel> BuscaPeloProduto(int id);
    }
}
