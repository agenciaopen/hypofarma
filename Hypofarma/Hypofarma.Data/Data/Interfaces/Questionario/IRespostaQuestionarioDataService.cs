﻿using Hypofarma.Data.Data.Interfaces.CRUD;
using Hypofarma.Data.Models.Questionario;
using System;
using System.Collections.Generic;

namespace Hypofarma.Data.Data.Interfaces.Questionario
{
    public interface IRespostaQuestionarioDataService : IAddService<RespostaQuestionarioModel>
    {
        List<RespostaQuestionarioModel> GetByDataCadastro(DateTime dthCadastro);
    }
}
