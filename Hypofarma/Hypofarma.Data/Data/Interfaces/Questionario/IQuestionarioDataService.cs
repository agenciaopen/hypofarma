﻿using Hypofarma.Data.Models.Questionario;
using System;
using System.Collections.Generic;

namespace Hypofarma.Data.Data.Interfaces.Questionario
{
    public interface IQuestionarioDataService
    {
        string GravaFarmacovigilancia(FarmacovigilanciaModel obj);

        List<FarmacovigilanciaModel> GetFarmacovigilancia(DateTime dthCadastro);
    }
}
