﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hypofarma.Data.DataAccess
{
    public interface ISqlDataAccess
    {
        Task<List<T>> LoadData<T, U>(string storedProcedure, U parameters);
        List<T> LoadDataSync<T, U>(string storedProcedure, U parameters);
        Task SaveData<T>(string storedProcedure, T parameters);

    }
}
