﻿using Dapper;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Hypofarma.Data.DataAccess
{
    sealed class SqlDataAccess : ISqlDataAccess
    {
        readonly IConfiguration _config;

        public SqlDataAccess(IConfiguration config) => _config = config;

        public async Task<List<T>> LoadData<T, U>(string storedProcedure, U parameters)
        {
            using (var connection = new MySqlConnection( _config.GetConnectionString("MySQL") ))
            {
                return (await connection.QueryAsync<T>(storedProcedure, parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
        }

        public List<T> LoadDataSync<T, U>(string storedProcedure, U parameters)
        {
            using (var connection = new MySqlConnection(_config.GetConnectionString("MySQL") ))
            {
                return (connection.Query<T>(storedProcedure, parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
        }

        public async Task SaveData<T>(string storedProcedure, T parameters)
        {
            using (IDbConnection connection = new MySqlConnection(_config.GetConnectionString("MySQL")))
            {
                await connection.QueryAsync(storedProcedure, parameters, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
