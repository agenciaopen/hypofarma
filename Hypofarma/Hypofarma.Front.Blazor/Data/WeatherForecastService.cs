using Hypofarma.Data.Data.Interfaces.Formulario;
using Hypofarma.Data.Data.Interfaces.Produto;
using Hypofarma.Data.Data.Interfaces.Rodape;
using Hypofarma.Data.Enum;
using Hypofarma.Data.Models.Formulario;
using Hypofarma.Data.Models.Rodape;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hypofarma.Front.Blazor.Data
{
    public class WeatherForecastService
    {
        readonly ICategoriaRegulatoriaDataService _categoria;
        readonly IClasseTerapeuticaDataService _classe;
        readonly IEmbalagemDataService _embalagem;
        readonly IPrincipioAtivoDataService _principioAtivo;
        readonly IProdutoDataService _produto;
        readonly IFormuarioDataService _formulario;
        readonly IRodapeDataService _rodape;

        public WeatherForecastService   (ICategoriaRegulatoriaDataService categoria,
                                         IClasseTerapeuticaDataService classe,
                                         IEmbalagemDataService embalagem,
                                         IPrincipioAtivoDataService principioAtivo,
                                         IProdutoDataService produto,
                                         IFormuarioDataService formulario,
                                         IRodapeDataService rodape)
        {
            _categoria = categoria;
            _classe = classe;
            _embalagem = embalagem;
            _principioAtivo = principioAtivo;
            _produto = produto;
            _formulario = formulario;

            _rodape = rodape;
        }


        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public Task<WeatherForecast[]> GetForecastAsync(DateTime startDate)
        {

            #region Teste

            //_categoria.Add( new CategoriaRegulatoriaModel() { DescCategoriaRegulatoria = "teste Categoria Regulatoria", NumUsuarioCadastro = 5});

            //_classe.Add( new ClasseTerapeuticaModel() { DescClasseTerapeutica = "teste Classe Terapeutica", NumUsuarioCadastro = 5});

            // _embalagem.Add(new EmbalagemModel() { DescEmbalagem = "teste Embalagem", NumUsuarioCadastro = 5, ArqIconeEmbalagem = ArquivoBinario.ConvertArquivoParaArrayByte(@"C:\Users\User\Pictures\Screenshots\02.jpg") });

            //_principioAtivo.Add( new PrincipioAtivoModel() { DescPrincipioAtivo = "Teste Principio Ativo", NumUsuarioCadastro = 5});

            //_produto.Add(new ProdutoModel()
            //{
            //    NomProduto = "Nome do Produto",
            //    Concentracao = "Concentracao do Produto",
            //    Apresentacao = "Apresentação do Produto",
            //    Registro = "Registro  do Produto",
            //    ImgBackGround = ArquivoBinario.ConvertArquivoParaArrayByte(@"C:\Users\User\Pictures\Screenshots\01.jpg"),
            //    Indicacao = "Indicação do Produto",
            //    IdcGenerico = false,
            //    IdcExibeVitrine = true,
            //    NumCategoriaRegulatoria = 2,
            //    NumClasseTerapeutica = 3,
            //    NumEmbalagem = 1,
            //    NumPrincipioAtivo = 4,
            //    NumUsuarioCadastro = 5
            //});

            //var _lst = _produto.GetById(1);

            //var _novoProduto = new ProdutoModel()
            //{
            //    Id = _lst.Id,
            //    NomProduto = _lst.NomProduto,
            //    Concentracao = _lst.Concentracao,
            //    Apresentacao = _lst.Apresentacao,
            //    Registro = _lst.Registro,
            //    ImgBackGround = _lst.ImgBackGround,
            //    Indicacao = _lst.Indicacao,
            //    IdcGenerico = true, //
            //    IdcExibeVitrine = _lst.IdcExibeVitrine,
            //    NumCategoriaRegulatoria = _lst.CategoriaRegulatoria.Id,
            //    NumClasseTerapeutica = _lst.ClasseTerapeutica.Id,
            //    NumEmbalagem = _lst.Embalagem.Id,
            //    NumPrincipioAtivo = _lst.PrincipioAtivo.Id,
            //    NumUsuarioCadastro = _lst.UsuarioCadastro.Id

            //};

            //_produto.Update(_novoProduto);

            //var _add = _formulario.GravaBannerPrincipal(new BannerPrincipalModel() { Titulo = "Titulo", LinkBotao = "xpto", TextoBotao = "TextoBotao" }, new List<ArquivoDataView>());

            //var _obj = _formulario.GetBannerPrincipal();

            //var _lst = _rodape.GetMidaWeb();

            _rodape.AddMidaWeb(new MidiaWebModel() { CanalWeb = CanalWebEnum.Instagram, LinkPerfil = "www.xpto.com.br" });

            //var _lst2 = _rodape.GetMidaWeb();

            //_rodape.RemoveMidaWeb(new MidiaWebModel() { CanalWeb = CanalWebEnum.Instagram, LinkPerfil = "www.xpto.com.br" });

            //var _lst3 = _rodape.GetMidaWeb();

            #endregion

            //var _lst1 = _rodape.GetRodape();

            //_rodape.GravaEnderecoRodape( new EnderecoRodapeModel() { Endereco1 = "xxx", Endereco2 = "yyyyy"});

            //var _lst2 = _rodape.GetRodape();

            var _r = _rodape.GetMidaWeb();
            var _r2 = _rodape.GetRodape();



            var rng = new Random();
            return Task.FromResult(Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = startDate.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            }).ToArray());
        }
    }
}
