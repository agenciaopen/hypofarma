window.initFaq = function () {
    $('.menu__open ul li a').click(function (e) {
        $('.trigger').click();
    });
      $(".trigger").on('click', function(){
          if($(".trigger-top").hasClass("no-animation")){
              $(".menu").toggleClass("menu__open");
              $(".trigger-top").removeClass("no-animation");
              $(".trigger-middle").toggleClass("d-none");
              $(".trigger-bottom").removeClass("no-animation");
              $(this).toggleClass('is-active');
          }
          else{
              $(this).toggleClass('is-active');
              setTimeout(function() { 
                  $(".menu").toggleClass("menu__open");
                  $(".trigger-middle").toggleClass("d-none");
              }, 100); 
          }
        });
    $('.collapse').on('shown.bs.collapse', function (e) {
        $(this).prev().addClass('active');
        var $card = $(this).closest('.card');
        var $open = $($(this).data('parent')).find('.collapse.show');
        
        var additionalOffset = 100;
        if($card.prevAll().filter($open.closest('.card')).length !== 0)
        {
              additionalOffset =  $open.height();
        }
        $('html,body').animate({
          scrollTop: $card.offset().top - additionalOffset
        }, 500);
    });
    $('.collapse').on('hidden.bs.collapse', function () {
        $(this).prev().removeClass('active');
    });
    if (window.location.hash) {
        var hash = window.location.hash;
        if ($(hash).length) {
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 900, 'swing');
        }
    }
}