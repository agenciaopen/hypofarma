window.initEmpreendimentos = function () {


    $(window).load(function() {
        // alert("window load occurred!");
        $(".gallery").css('opacity','1');
    });
    $(".consultor__emp").addClass('chatemp');

    if($(".consultor__emp").hasClass("chatemp")){
        $("#anapro").text('');
        let chatAnapro = ($('.anaprochat').text());
        $("#anaproemp").text(chatAnapro);
        $(".chatemp").attr("href", chatAnapro);
        document.getElementById('chat__anapro').setAttribute("onclick", "window.open('" + chatAnapro + "','popup','width=600,height=600'); return false;");   
        $(".chat__anaprobtn").attr("href", chatAnapro); 
        document.getElementById('chat__anaprobtn').setAttribute("onclick", "window.open('" + chatAnapro + "','popup','width=600,height=600'); return false;");   
        document.getElementById('chat__anaprobtnnav').setAttribute("onclick", "window.open('" + chatAnapro + "','popup','width=600,height=600'); return false;");   

    }
    // var slug = function(str) {
    //     var $slug = '';
    //     var trimmed = $.trim(str);
    //     $slug = trimmed.replace(/[^a-z0-9-]/gi, '-').
    //     replace(/-+/g, '-').
    //     replace(/^-|-$/g, '');
    //     return $slug.toLowerCase();
    // }
    var slug = function(str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();
      
        // remove accents, swap ñ for n, etc
        var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
        var to   = "aaaaaeeeeeiiiiooooouuuunc------";
        for (var i=0, l=from.length ; i<l ; i++) {
          str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }
      
        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
          .replace(/\s+/g, '-') // collapse whitespace and replace by -
          .replace(/-+/g, '-'); // collapse dashes
      
        return str;
      };
    // $("a").attr("href", "http://www.google.com/")
    //$(".bread_crumb").css({/*position:  'fixed', 'z-index': '999', */ 'width' : '100%'});   // or any changing value

    let estadoSlug = (slug($('#estado').text()));
    let cidadeSlug = (slug($('#cidade').text()));
    let bairroSlug = (slug($('#bairro').text()));
    let estado = ($('#estado').text());
    let cidade = ($('#cidade').text());
    let bairro = ($('#bairro').text());
    let empreendimentoNome = ($('#nomeemp').text());
    $(".navbar-brand, .header a").attr("href", estado);
    $(".nomeestado a").attr("href", estadoSlug);
    $(".nomecidade a").attr("href", ""+estadoSlug+"/busca?&cidade=" +cidadeSlug+ "&bairro=&quartos=&statusEmpreendimento=&tipoEmpreendimento=&modeloNegocio=");
    $(".nomebairro a").attr("href", ""+estadoSlug+"/busca?&cidade=" +cidadeSlug+ "&bairro=" + bairroSlug+ "&quartos=&statusEmpreendimento=&tipoEmpreendimento=&modeloNegocio=");
    $(".nomeestado a").text(estado);
    $(".nomecidade a").text(cidade);
    $(".nomebairro a").text(bairro);
    $(".nomeemp").text(empreendimentoNome);

    // alert(estado);
    // alert(cidade);
    // alert(bairro);
    // $(window).load(function(){
        $("#galeria__slider").lightSlider({
            item: 1,
            autoWidth: false,
            slideMove: 1, // slidemove will be 1 if loop is true
            slideMargin: 10,
    
            addClass: '',
            mode: "slide",
            useCSS: true,
            cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
            easing: 'linear', //'for jquery animation',////
    
            speed: 400, //ms'

            auto: false,
            loop: false,
            slideEndAnimation: true,
            pause: 2000,
    
            keyPress: true,
            controls: true,
            prevHtml: '',
            nextHtml: '',
    
            rtl:false,
            adaptiveHeight:false,
    
            enableTouch:true,
            enableDrag:true,
            freeMove:true,
            swipeThreshold: 40,
    
            responsive : [],
        });
        $('#galeria__slider').lightGallery({
            download: false,
                        thumbnail: false,
            pullCaptionUp: true,
            subHtmlSelectorRelative: true
        });
        
        $("#gallery__slider").lightSlider({
            item: 2,
            autoWidth: false,
            slideMove: 1, // slidemove will be 1 if loop is true
            slideMargin: 10,
    
            addClass: '',
            mode: "slide",
            useCSS: true,
            cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
            easing: 'linear', //'for jquery animation',////
    
            speed: 400, //ms'

            auto: false,
            loop: false,
            slideEndAnimation: true,
            pause: 2000,
    
            keyPress: true,
            controls: true,
            prevHtml: '',
            nextHtml: '',
    
            rtl:false,
            adaptiveHeight:false,
    
            enableTouch:true,
            enableDrag:true,
            freeMove:true,
            swipeThreshold: 40,
    
            responsive : [
                {
                    breakpoint:767,
                    settings: {
                        item:1,
                        slideMove:1
                    }
                }
            ],
        });
        if (screen.width > 992) {
            let navHeight = $('.navbar_main').height();

            $('#fixed_breadcrumb').css({ top : navHeight + 38 + 'px' });
        }
        $('#gallery__slider').lightGallery({
            download: false,
                        thumbnail: false,
            pullCaptionUp: true,
            subHtmlSelectorRelative: true
        });

        $("#plan__slider").lightSlider({
            item: 2,
            autoWidth: false,
            slideMove: 1, // slidemove will be 1 if loop is true
            slideMargin: 10,
    
            addClass: '',
            mode: "slide",
            useCSS: true,
            cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
            easing: 'linear', //'for jquery animation',////
    
            speed: 400, //ms'

            auto: false,
            loop: false,
            slideEndAnimation: true,
            pause: 2000,
    
            keyPress: true,
            controls: true,
            prevHtml: '',
            nextHtml: '',
    
            rtl:false,
            adaptiveHeight:false,
    
            enableTouch:true,
            enableDrag:true,
            freeMove:true,
            swipeThreshold: 40,
    
            responsive : [
                {
                    breakpoint:767,
                    settings: {
                        item:1,
                        slideMove:1
                    }
                }
            ],
        });
        $('#plan__slider').lightGallery({
            download: false,
                        thumbnail: false,
            pullCaptionUp: true,
            subHtmlSelectorRelative: true
        });
        $('span.play-video').click(function(){
            $('.youtube-video iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
        });
        
        $('span.stop-video').click(function(){
            $('.youtube-video iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
        });
        
        $('span.pause-video').click(function(){
            $('.youtube-video iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
        });

        $.fn.isInViewport = function() {
            var elementTop = $(this).offset().top;
            var elementBottom = elementTop + $(this).outerHeight();
        
            var viewportTop = $(window).scrollTop();
            var viewportBottom = viewportTop + $(window).height();
        
            return elementBottom > viewportTop && elementTop < viewportBottom;
        };
    // }); 
    function statusActive(){
        $('.status').addClass('status-animated');    
        $('.status__item').addClass('status__item-animated');  
    
        $('.status-animated .status__item .counter').each(function() {
            var $this = $(this),
                countTo = $this.attr('data-count');        
            $({ countNum: $this.text()}).animate({
            countNum: countTo
            },
            {
            duration: 1000,
            easing:'linear',
            step: function() {
                $this.text(Math.floor(this.countNum));
            },
            complete: function() {
                $this.text(this.countNum + '%');
                //alert('finished');
                $('.status-animated .status__item .counter').removeClass('counter');
            }
            });  
        });         
    };
    function videoActive(){
        $('.video').addClass('video__active');    
        $( ".video .poster, .video .video__icon" ).click(function() {
            $('.video').addClass('video__active-click');   
            $('.youtube-video iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*'); 
        });
    }

    $(window).on('resize scroll', function() {
        let cta = $('.cta');
        let ctaTop = 250;
        if ($(window).scrollTop() >= ctaTop) {
            // topBar.removeClass('fixed-top');
            cta.addClass('cta__scrolled');
    
        } else {
            cta.removeClass('cta__scrolled');
        }
        //console.log('200');
    // $(window).on('resize scroll', function() {
        let status__listTrue = $('.status__list');
        if (status__listTrue.length) {
          if ($('.status__list').isInViewport()) {
              statusActive();
          }
          else {
              $('.status').removeClass('status-animated');    
              $('.status__item').removeClass('status__item-animated');
              $('.status .status__item .progress + label').addClass('counter');
          }
        }
        let videoTrue = $('.video');
        if (videoTrue.length) {
          if ($('.video').isInViewport()) {
              videoActive();
          }
          else {
              $('.youtube-video iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
              $('.video').removeClass('video__active');   
              $('.video').removeClass('video__active-click');   
          }
        }
    });

    $('.services li').click(function(){
    if ($(this).hasClass('active')) {
        console.log('clicked');
        $(this).removeClass('active');
    } else {
            $(this).addClass('active');
        }
    });

    $('.menu__open ul li a').click(function (e) {
        $('.trigger').click();
    });
      $(".trigger").on('click', function(){
          if($(".trigger-top").hasClass("no-animation")){
              $(".menu").toggleClass("menu__open");
              $(".trigger-top").removeClass("no-animation");
              $(".trigger-middle").toggleClass("d-none");
              $(".trigger-bottom").removeClass("no-animation");
              $(this).toggleClass('is-active');
          }
          else{
              $(this).toggleClass('is-active');
              setTimeout(function() { 
                  $(".menu").toggleClass("menu__open");
                  $(".trigger-middle").toggleClass("d-none");
              }, 100); 
          }
        });

}